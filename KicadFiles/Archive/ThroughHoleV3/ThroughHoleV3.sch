EESchema Schematic File Version 4
LIBS:24VMotor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_Motor:L298HN Mtr12
U 1 1 5BD22F41
P 6400 2050
F 0 "Mtr12" H 6400 2150 50  0000 C CNN
F 1 "L298HN" H 6450 2050 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 6450 1400 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 6550 2300 50  0001 C CNN
	1    6400 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5BD2317D
P 6700 1150
F 0 "C2" H 6815 1196 50  0000 L CNN
F 1 "100n" H 6815 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6738 1000 50  0001 C CNN
F 3 "~" H 6700 1150 50  0001 C CNN
	1    6700 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5BD23229
P 6200 1150
F 0 "C1" H 6000 1200 50  0000 L CNN
F 1 "100n" H 5900 1100 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6238 1000 50  0001 C CNN
F 3 "~" H 6200 1150 50  0001 C CNN
	1    6200 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5BD23435
P 6200 3100
F 0 "R10" V 6200 3025 50  0000 L CNN
F 1 "0.15" H 6250 3100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6130 3100 50  0001 C CNN
F 3 "~" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5BD234C3
P 6100 3100
F 0 "R9" V 6100 3050 50  0000 L CNN
F 1 "0.15" H 5875 3100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6030 3100 50  0001 C CNN
F 3 "~" H 6100 3100 50  0001 C CNN
	1    6100 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:D D7
U 1 1 5BD23561
P 8950 1600
F 0 "D7" H 8950 1500 50  0000 C CNN
F 1 "D" H 9050 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 1600 50  0001 C CNN
F 3 "~" H 8950 1600 50  0001 C CNN
	1    8950 1600
	0    1    1    0   
$EndComp
$Comp
L Device:D D5
U 1 1 5BD235ED
P 8700 1600
F 0 "D5" H 8700 1500 50  0000 C CNN
F 1 "D" H 8800 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 1600 50  0001 C CNN
F 3 "~" H 8700 1600 50  0001 C CNN
	1    8700 1600
	0    1    1    0   
$EndComp
$Comp
L Device:D D3
U 1 1 5BD2361F
P 8450 1600
F 0 "D3" H 8450 1500 50  0000 C CNN
F 1 "D" H 8550 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 1600 50  0001 C CNN
F 3 "~" H 8450 1600 50  0001 C CNN
	1    8450 1600
	0    1    1    0   
$EndComp
$Comp
L Device:D D1
U 1 1 5BD23651
P 8200 1600
F 0 "D1" H 8200 1500 50  0000 C CNN
F 1 "D" H 8300 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 1600 50  0001 C CNN
F 3 "~" H 8200 1600 50  0001 C CNN
	1    8200 1600
	0    1    1    0   
$EndComp
$Comp
L Device:D D8
U 1 1 5BD23B43
P 8950 2600
F 0 "D8" H 8950 2500 50  0000 C CNN
F 1 "D" H 9050 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 2600 50  0001 C CNN
F 3 "~" H 8950 2600 50  0001 C CNN
	1    8950 2600
	0    1    1    0   
$EndComp
$Comp
L Device:D D6
U 1 1 5BD23B4A
P 8700 2600
F 0 "D6" H 8700 2500 50  0000 C CNN
F 1 "D" H 8800 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 2600 50  0001 C CNN
F 3 "~" H 8700 2600 50  0001 C CNN
	1    8700 2600
	0    1    1    0   
$EndComp
$Comp
L Device:D D4
U 1 1 5BD23B51
P 8450 2600
F 0 "D4" H 8450 2500 50  0000 C CNN
F 1 "D" H 8550 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 2600 50  0001 C CNN
F 3 "~" H 8450 2600 50  0001 C CNN
	1    8450 2600
	0    1    1    0   
$EndComp
$Comp
L Device:D D2
U 1 1 5BD23B58
P 8200 2600
F 0 "D2" H 8200 2500 50  0000 C CNN
F 1 "D" H 8300 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 2600 50  0001 C CNN
F 3 "~" H 8200 2600 50  0001 C CNN
	1    8200 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 2750 6100 2800
Wire Wire Line
	6200 2750 6200 2900
Wire Wire Line
	6100 3250 6100 3300
Wire Wire Line
	6200 3250 6200 3300
Wire Wire Line
	6100 2800 5800 2800
Connection ~ 6100 2800
Wire Wire Line
	6100 2800 6100 2950
Wire Wire Line
	6200 2900 5800 2900
Connection ~ 6200 2900
Wire Wire Line
	6200 2900 6200 2950
Wire Wire Line
	6500 1350 6500 1000
Wire Wire Line
	6500 1000 6700 1000
Wire Wire Line
	6400 1000 6400 1350
Wire Wire Line
	8200 2750 8450 2750
Wire Wire Line
	8700 2750 8950 2750
Wire Wire Line
	8200 2750 8200 2900
Wire Wire Line
	8450 2750 8700 2750
Wire Wire Line
	8200 1450 8450 1450
Wire Wire Line
	8450 1450 8700 1450
Wire Wire Line
	8700 1450 8950 1450
$Comp
L Connector:Conn_01x06_Male Mtr1
U 1 1 5BD176E9
P 9400 1650
F 0 "Mtr1" H 9500 2100 50  0000 C CNN
F 1 "Conn_01x06_Male" H 9550 2000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9400 1650 50  0001 C CNN
F 3 "~" H 9400 1650 50  0001 C CNN
	1    9400 1650
	1    0    0    -1  
$EndComp
Text Label 9600 1450 0    50   ~ 0
EncB1
Text Label 9600 1550 0    50   ~ 0
EncA1
Text Label 9600 1650 0    50   ~ 0
5V4
Text Label 9600 1750 0    50   ~ 0
GND1
Text Label 9600 1850 0    50   ~ 0
OutA1
Text Label 9600 1950 0    50   ~ 0
OutB1
$Comp
L Connector:Conn_01x06_Male Mtr2
U 1 1 5BD17A19
P 10300 1650
F 0 "Mtr2" H 10400 2100 50  0000 C CNN
F 1 "Conn_01x06_Male" H 10400 2000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 10300 1650 50  0001 C CNN
F 3 "~" H 10300 1650 50  0001 C CNN
	1    10300 1650
	1    0    0    -1  
$EndComp
Text Label 10500 1450 0    50   ~ 0
EncB2
Text Label 10500 1550 0    50   ~ 0
EncA2
Text Label 10500 1650 0    50   ~ 0
5V4
Text Label 10500 1750 0    50   ~ 0
GND1
Text Label 10500 1850 0    50   ~ 0
OutA2
Text Label 10500 1950 0    50   ~ 0
OutB2
$Comp
L Connector:Conn_01x06_Male Mtr3
U 1 1 5BD17BFC
P 9400 4450
F 0 "Mtr3" H 9500 4900 50  0000 C CNN
F 1 "Conn_01x06_Male" H 9550 4800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9400 4450 50  0001 C CNN
F 3 "~" H 9400 4450 50  0001 C CNN
	1    9400 4450
	1    0    0    -1  
$EndComp
Text Label 9600 4250 0    50   ~ 0
EncB3
Text Label 9600 4350 0    50   ~ 0
EncA3
Text Label 9600 4450 0    50   ~ 0
5V1
Text Label 9600 4550 0    50   ~ 0
GND3
Text Label 9600 4650 0    50   ~ 0
OutA3
Text Label 9600 4750 0    50   ~ 0
OutB3
$Comp
L Connector:Conn_01x06_Male Mtr4
U 1 1 5BD17DE0
P 10250 4450
F 0 "Mtr4" H 10350 4900 50  0000 C CNN
F 1 "Conn_01x06_Male" H 10400 4800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 10250 4450 50  0001 C CNN
F 3 "~" H 10250 4450 50  0001 C CNN
	1    10250 4450
	1    0    0    -1  
$EndComp
Text Label 10450 4250 0    50   ~ 0
EncB4
Text Label 10450 4350 0    50   ~ 0
EncA4
Text Label 10450 4450 0    50   ~ 0
5V1
Text Label 10450 4550 0    50   ~ 0
GND3
Text Label 10450 4650 0    50   ~ 0
OutA4
Text Label 10450 4750 0    50   ~ 0
OutB4
Connection ~ 8450 1950
Connection ~ 8200 1850
$Comp
L Device:LED_Dual_2pin M1
U 1 1 5BD1AA69
P 7650 1450
F 0 "M1" H 7650 1846 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 1755 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 1450 50  0001 C CNN
F 3 "~" H 7650 1450 50  0001 C CNN
	1    7650 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Dual_2pin M2
U 1 1 5BD1AAB9
P 7650 2750
F 0 "M2" H 7650 2500 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 2400 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 2750 50  0001 C CNN
F 3 "~" H 7650 2750 50  0001 C CNN
	1    7650 2750
	1    0    0    -1  
$EndComp
Connection ~ 8200 2750
Connection ~ 8450 2750
Connection ~ 8700 2750
Connection ~ 8700 2150
Connection ~ 8950 2250
$Comp
L power:+12V #PWR024
U 1 1 5BD26E43
P 6700 1000
F 0 "#PWR024" H 6700 850 50  0001 C CNN
F 1 "+12V" H 6715 1173 50  0000 C CNN
F 2 "" H 6700 1000 50  0001 C CNN
F 3 "" H 6700 1000 50  0001 C CNN
	1    6700 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5BD26F0C
P 675 550
F 0 "J1" V 675 600 50  0000 L CNN
F 1 "Screw" V 750 400 50  0000 L CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x02_P3.50mm_Vertical" H 675 550 50  0001 C CNN
F 3 "~" H 675 550 50  0001 C CNN
	1    675  550 
	0    1    -1   0   
$EndComp
Connection ~ 8450 1450
Connection ~ 8700 1450
Text Label 7000 2250 0    50   ~ 0
OutB2
Text Label 7000 2150 0    50   ~ 0
OutA2
Text Label 7000 1950 0    50   ~ 0
OutB1
Text Label 7000 1850 0    50   ~ 0
OutA1
Wire Wire Line
	7000 1850 7950 1850
Wire Wire Line
	7000 1950 7350 1950
Wire Wire Line
	7000 2150 7950 2150
Wire Wire Line
	7000 2250 7350 2250
Wire Wire Line
	8450 1750 8450 1950
Wire Wire Line
	8450 1950 8450 2450
Wire Wire Line
	8700 1750 8700 2150
Wire Wire Line
	8700 2150 8700 2450
Wire Wire Line
	8950 1750 8950 2250
Wire Wire Line
	8950 2250 8950 2450
Wire Wire Line
	8200 1850 8200 2450
Wire Wire Line
	8200 1750 8200 1850
$Comp
L Device:R R13
U 1 1 5BD2FD5A
P 7350 1600
F 0 "R13" H 7200 1600 50  0000 L CNN
F 1 "560" H 7325 1600 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 1600 50  0001 C CNN
F 3 "~" H 7350 1600 50  0001 C CNN
	1    7350 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5BD2FDDC
P 7350 2600
F 0 "R14" H 7200 2600 50  0000 L CNN
F 1 "560" H 7325 2600 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 2600 50  0001 C CNN
F 3 "~" H 7350 2600 50  0001 C CNN
	1    7350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2450 7350 2250
Connection ~ 7350 2250
Wire Wire Line
	7350 2250 8950 2250
Wire Wire Line
	7950 2750 7950 2150
Connection ~ 7950 2150
Wire Wire Line
	7950 2150 8700 2150
Wire Wire Line
	7350 1750 7350 1950
Connection ~ 7350 1950
Wire Wire Line
	7350 1950 8450 1950
Wire Wire Line
	7950 1450 7950 1850
Connection ~ 7950 1850
Wire Wire Line
	7950 1850 8200 1850
Connection ~ 6700 1000
$Comp
L Driver_Motor:L298HN Mtr34
U 1 1 5BD37B78
P 6400 4850
F 0 "Mtr34" H 6400 4950 50  0000 C CNN
F 1 "L298HN" H 6450 4850 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 6450 4200 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 6550 5100 50  0001 C CNN
	1    6400 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5BD37B7F
P 6700 3950
F 0 "C5" H 6815 3996 50  0000 L CNN
F 1 "100n" H 6815 3905 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6738 3800 50  0001 C CNN
F 3 "~" H 6700 3950 50  0001 C CNN
	1    6700 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5BD37B86
P 6200 3950
F 0 "C4" H 6000 4000 50  0000 L CNN
F 1 "100n" H 5900 3900 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6238 3800 50  0001 C CNN
F 3 "~" H 6200 3950 50  0001 C CNN
	1    6200 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5BD37B8D
P 6200 5900
F 0 "R12" V 6200 5825 50  0000 L CNN
F 1 "0.15" H 6250 5900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6130 5900 50  0001 C CNN
F 3 "~" H 6200 5900 50  0001 C CNN
	1    6200 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5BD37B94
P 6100 5900
F 0 "R11" V 6100 5825 50  0000 L CNN
F 1 "0.15" H 5875 5900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6030 5900 50  0001 C CNN
F 3 "~" H 6100 5900 50  0001 C CNN
	1    6100 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:D D15
U 1 1 5BD37B9B
P 8950 4400
F 0 "D15" H 8950 4300 50  0000 C CNN
F 1 "D" H 9050 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 4400 50  0001 C CNN
F 3 "~" H 8950 4400 50  0001 C CNN
	1    8950 4400
	0    1    1    0   
$EndComp
$Comp
L Device:D D13
U 1 1 5BD37BA2
P 8700 4400
F 0 "D13" H 8700 4300 50  0000 C CNN
F 1 "D" H 8800 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 4400 50  0001 C CNN
F 3 "~" H 8700 4400 50  0001 C CNN
	1    8700 4400
	0    1    1    0   
$EndComp
$Comp
L Device:D D11
U 1 1 5BD37BA9
P 8450 4400
F 0 "D11" H 8450 4300 50  0000 C CNN
F 1 "D" H 8550 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 4400 50  0001 C CNN
F 3 "~" H 8450 4400 50  0001 C CNN
	1    8450 4400
	0    1    1    0   
$EndComp
$Comp
L Device:D D9
U 1 1 5BD37BB0
P 8200 4400
F 0 "D9" H 8200 4300 50  0000 C CNN
F 1 "D" H 8300 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 4400 50  0001 C CNN
F 3 "~" H 8200 4400 50  0001 C CNN
	1    8200 4400
	0    1    1    0   
$EndComp
$Comp
L Device:D D16
U 1 1 5BD37BB7
P 8950 5400
F 0 "D16" H 8950 5300 50  0000 C CNN
F 1 "D" H 9050 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 5400 50  0001 C CNN
F 3 "~" H 8950 5400 50  0001 C CNN
	1    8950 5400
	0    1    1    0   
$EndComp
$Comp
L Device:D D14
U 1 1 5BD37BBE
P 8700 5400
F 0 "D14" H 8700 5300 50  0000 C CNN
F 1 "D" H 8800 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 5400 50  0001 C CNN
F 3 "~" H 8700 5400 50  0001 C CNN
	1    8700 5400
	0    1    1    0   
$EndComp
$Comp
L Device:D D12
U 1 1 5BD37BC5
P 8450 5400
F 0 "D12" H 8450 5300 50  0000 C CNN
F 1 "D" H 8550 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 5400 50  0001 C CNN
F 3 "~" H 8450 5400 50  0001 C CNN
	1    8450 5400
	0    1    1    0   
$EndComp
$Comp
L Device:D D10
U 1 1 5BD37BCC
P 8200 5400
F 0 "D10" H 8200 5300 50  0000 C CNN
F 1 "D" H 8300 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 5400 50  0001 C CNN
F 3 "~" H 8200 5400 50  0001 C CNN
	1    8200 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 5550 6100 5600
Wire Wire Line
	6200 5550 6200 5700
Wire Wire Line
	6100 6050 6100 6100
Wire Wire Line
	6200 6050 6200 6100
Wire Wire Line
	6100 5600 5800 5600
Connection ~ 6100 5600
Wire Wire Line
	6100 5600 6100 5750
Wire Wire Line
	6200 5700 5800 5700
Connection ~ 6200 5700
Wire Wire Line
	6200 5700 6200 5750
Wire Wire Line
	6500 4150 6500 3800
Wire Wire Line
	6500 3800 6700 3800
Wire Wire Line
	6200 3800 6400 3800
Wire Wire Line
	6400 3800 6400 4150
Wire Wire Line
	8200 5550 8450 5550
Wire Wire Line
	8700 5550 8950 5550
Wire Wire Line
	8200 5550 8200 5700
Wire Wire Line
	8450 5550 8700 5550
Wire Wire Line
	8200 4250 8450 4250
Wire Wire Line
	8450 4250 8700 4250
Wire Wire Line
	8700 4250 8950 4250
Connection ~ 8450 4750
Connection ~ 8200 4650
$Comp
L Device:LED_Dual_2pin M3
U 1 1 5BD37C08
P 7650 4250
F 0 "M3" H 7650 4646 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 4555 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 4250 50  0001 C CNN
F 3 "~" H 7650 4250 50  0001 C CNN
	1    7650 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Dual_2pin M4
U 1 1 5BD37C0F
P 7650 5550
F 0 "M4" H 7650 5300 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 5200 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 5550 50  0001 C CNN
F 3 "~" H 7650 5550 50  0001 C CNN
	1    7650 5550
	1    0    0    -1  
$EndComp
Connection ~ 8200 5550
Connection ~ 8450 5550
Connection ~ 8700 5550
Connection ~ 8700 4950
Connection ~ 8950 5050
$Comp
L power:+12V #PWR026
U 1 1 5BD37C21
P 6700 3800
F 0 "#PWR026" H 6700 3650 50  0001 C CNN
F 1 "+12V" H 6715 3973 50  0000 C CNN
F 2 "" H 6700 3800 50  0001 C CNN
F 3 "" H 6700 3800 50  0001 C CNN
	1    6700 3800
	1    0    0    -1  
$EndComp
Connection ~ 8450 4250
Connection ~ 8700 4250
Text Label 7000 5050 0    50   ~ 0
OutB4
Text Label 7000 4950 0    50   ~ 0
OutA4
Text Label 7000 4750 0    50   ~ 0
OutB3
Text Label 7000 4650 0    50   ~ 0
OutA3
Wire Wire Line
	7000 4650 7950 4650
Wire Wire Line
	7000 4750 7350 4750
Wire Wire Line
	7000 4950 7950 4950
Wire Wire Line
	7000 5050 7350 5050
Wire Wire Line
	8450 4550 8450 4750
Wire Wire Line
	8450 4750 8450 5250
Wire Wire Line
	8700 4550 8700 4950
Wire Wire Line
	8700 4950 8700 5250
Wire Wire Line
	8950 4550 8950 5050
Wire Wire Line
	8950 5050 8950 5250
Wire Wire Line
	8200 4650 8200 5250
Wire Wire Line
	8200 4550 8200 4650
$Comp
L Device:R R15
U 1 1 5BD37C39
P 7350 4400
F 0 "R15" V 7350 4325 50  0000 L CNN
F 1 "560" H 7150 4400 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 4400 50  0001 C CNN
F 3 "~" H 7350 4400 50  0001 C CNN
	1    7350 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5BD37C40
P 7350 5400
F 0 "R16" V 7350 5325 50  0000 L CNN
F 1 "560" H 7150 5400 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 5400 50  0001 C CNN
F 3 "~" H 7350 5400 50  0001 C CNN
	1    7350 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 5250 7350 5050
Connection ~ 7350 5050
Wire Wire Line
	7350 5050 8950 5050
Wire Wire Line
	7950 5550 7950 4950
Connection ~ 7950 4950
Wire Wire Line
	7950 4950 8700 4950
Wire Wire Line
	7350 4550 7350 4750
Connection ~ 7350 4750
Wire Wire Line
	7350 4750 8450 4750
Wire Wire Line
	7950 4250 7950 4650
Connection ~ 7950 4650
Wire Wire Line
	7950 4650 8200 4650
Connection ~ 6700 3800
Text Label 5800 1750 2    50   ~ 0
D6-EnA1
Text Label 5800 2150 2    50   ~ 0
D7-EnB1
Text Label 5800 4950 2    50   ~ 0
D9-EnB2
Text Label 5800 4550 2    50   ~ 0
D8-EnA2
Text Label 4450 2575 0    50   ~ 0
D22-In11
Text Label 4450 2675 0    50   ~ 0
D23-In21
Text Label 4450 2775 0    50   ~ 0
D24-In31
Text Label 4450 2875 0    50   ~ 0
D25-In41
Text Label 5800 1550 2    50   ~ 0
D22-In11
Text Label 5800 1650 2    50   ~ 0
D23-In21
Text Label 5800 1950 2    50   ~ 0
D24-In31
Text Label 5800 2050 2    50   ~ 0
D25-In41
Text Label 5800 4350 2    50   ~ 0
D26-In12
Text Label 5800 4450 2    50   ~ 0
D27-In22
Text Label 5800 4750 2    50   ~ 0
D28-In32
Text Label 5800 4850 2    50   ~ 0
D29-In42
Text Label 5800 2800 2    50   ~ 0
SenseA1-AmpA1
Text Label 5800 2900 2    50   ~ 0
SenseB1-AmpB1
Text Label 5800 5600 2    50   ~ 0
SenseA2-AmpA2
Text Label 5800 5700 2    50   ~ 0
SenseB2-AmpB2
Text Label 1850 2675 2    50   ~ 0
AmpA1-A0
Text Label 1850 2775 2    50   ~ 0
AmpB1-A1
Text Label 4450 2975 0    50   ~ 0
D26-In12
Text Label 4450 3075 0    50   ~ 0
D27-In22
Text Label 4450 3175 0    50   ~ 0
D28-In32
Text Label 4450 3275 0    50   ~ 0
D29-In42
Text Label 1850 2875 2    50   ~ 0
AmpA2-A2
Text Label 1850 2975 2    50   ~ 0
AmpB2-A3
Wire Notes Line
	9200 1100 10000 1100
Wire Notes Line
	10000 1100 10000 2050
Wire Notes Line
	10000 2050 9200 2050
Wire Notes Line
	9200 2050 9200 1100
Wire Notes Line
	10050 1100 10050 2050
Wire Notes Line
	10050 2050 10900 2050
Wire Notes Line
	10900 2050 10900 1100
Wire Notes Line
	10900 1100 10050 1100
Wire Notes Line
	10050 3950 10900 3950
Wire Notes Line
	10900 3950 10900 4800
Wire Notes Line
	10900 4800 10050 4800
Wire Notes Line
	10050 4800 10050 3950
Wire Notes Line
	10000 3950 10000 4800
Wire Notes Line
	10000 4800 9200 4800
Wire Notes Line
	9200 4800 9200 3950
Wire Notes Line
	9200 3950 10000 3950
Text Label 9200 3950 0    50   ~ 0
Motor3
Text Label 10050 3950 0    50   ~ 0
Motor4
Text Label 9200 1100 0    50   ~ 0
Motor1
Text Label 10050 1100 0    50   ~ 0
Motor2
NoConn ~ 1850 3275
NoConn ~ 1850 4275
NoConn ~ 1850 4575
NoConn ~ 1850 5675
NoConn ~ 2900 925 
NoConn ~ 3000 925 
NoConn ~ 3100 925 
NoConn ~ 3200 925 
NoConn ~ 3300 925 
NoConn ~ 3400 925 
NoConn ~ 1850 5275
$Comp
L Connector:Conn_01x04_Female US1
U 1 1 5BE4CD47
P 1150 1575
F 0 "US1" H 525 1750 50  0000 L CNN
F 1 "Ultrasonic1" V 1200 1325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 1575 50  0001 C CNN
F 3 "~" H 1150 1575 50  0001 C CNN
	1    1150 1575
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US2
U 1 1 5BE4D00E
P 1150 2075
F 0 "US2" H 500 2200 50  0000 L CNN
F 1 "Ultrasonic2" V 1200 1825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 2075 50  0001 C CNN
F 3 "~" H 1150 2075 50  0001 C CNN
	1    1150 2075
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US3
U 1 1 5BE4D0A0
P 1150 2575
F 0 "US3" H 500 2725 50  0000 L CNN
F 1 "Ultrasonic3" V 1200 2325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 2575 50  0001 C CNN
F 3 "~" H 1150 2575 50  0001 C CNN
	1    1150 2575
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US4
U 1 1 5BE4D13E
P 1150 3075
F 0 "US4" H 500 3200 50  0000 L CNN
F 1 "Ultrasonic4" V 1200 2825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 3075 50  0001 C CNN
F 3 "~" H 1150 3075 50  0001 C CNN
	1    1150 3075
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US5
U 1 1 5BE4D1C0
P 1150 3575
F 0 "US5" H 475 3700 50  0000 L CNN
F 1 "Ultrasonic5" V 1200 3325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 3575 50  0001 C CNN
F 3 "~" H 1150 3575 50  0001 C CNN
	1    1150 3575
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US6
U 1 1 5BE4D258
P 1150 4075
F 0 "US6" H 500 4225 50  0000 L CNN
F 1 "Ultrasonic6" V 1200 3825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 4075 50  0001 C CNN
F 3 "~" H 1150 4075 50  0001 C CNN
	1    1150 4075
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US7
U 1 1 5BE4D2E2
P 1150 4575
F 0 "US7" H 500 4700 50  0000 L CNN
F 1 "Ultrasonic7" V 1200 4325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 4575 50  0001 C CNN
F 3 "~" H 1150 4575 50  0001 C CNN
	1    1150 4575
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female US8
U 1 1 5BE4D36A
P 1150 5075
F 0 "US8" H 500 5225 50  0000 L CNN
F 1 "Ultrasonic8" V 1200 4825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 5075 50  0001 C CNN
F 3 "~" H 1150 5075 50  0001 C CNN
	1    1150 5075
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR01
U 1 1 5BD276DF
P 1625 675
F 0 "#PWR01" H 1625 525 50  0001 C CNN
F 1 "+12V" H 1640 848 50  0000 C CNN
F 2 "" H 1625 675 50  0001 C CNN
F 3 "" H 1625 675 50  0001 C CNN
	1    1625 675 
	1    0    0    -1  
$EndComp
Text Label 950  1475 2    50   ~ 0
5V3
Text Label 950  1975 2    50   ~ 0
5V3
Text Label 950  2475 2    50   ~ 0
5V3
Text Label 950  2975 2    50   ~ 0
5V3
Text Label 950  3475 2    50   ~ 0
5V3
Text Label 950  3975 2    50   ~ 0
5V3
Text Label 950  4475 2    50   ~ 0
5V3
Text Label 950  4975 2    50   ~ 0
5V3
Text Label 950  1775 2    50   ~ 0
GND5
Text Label 950  2275 2    50   ~ 0
GND5
Text Label 950  2775 2    50   ~ 0
GND5
Text Label 950  3275 2    50   ~ 0
GND5
Text Label 950  3775 2    50   ~ 0
GND5
Text Label 950  4275 2    50   ~ 0
GND5
Text Label 950  4775 2    50   ~ 0
GND5
Text Label 950  5275 2    50   ~ 0
GND5
Text Label 950  1575 2    50   ~ 0
D49-Tr1
Text Label 950  2075 2    50   ~ 0
D48-Tr2
Text Label 950  2575 2    50   ~ 0
D47-Tr3
Text Label 950  3075 2    50   ~ 0
D46-Tr4
Text Label 950  3575 2    50   ~ 0
D45-Tr5
Text Label 950  4075 2    50   ~ 0
D44-Tr6
Text Label 950  4575 2    50   ~ 0
D43-Tr7
Text Label 950  5075 2    50   ~ 0
D42-Tr8
Text Label 950  1675 2    50   ~ 0
Ech1-A8
Text Label 950  2175 2    50   ~ 0
Ech2-A9
Text Label 950  2675 2    50   ~ 0
Ech3-A10
Text Label 950  3175 2    50   ~ 0
Ech4-A11
Text Label 950  3675 2    50   ~ 0
Ech5-A12
Text Label 950  4175 2    50   ~ 0
Ech6-A13
Text Label 950  4675 2    50   ~ 0
Ech7-A14
Text Label 950  5175 2    50   ~ 0
Ech8-A15
Text Label 4450 5075 0    50   ~ 0
D47-Tr3
Text Label 4450 5175 0    50   ~ 0
D48-Tr2
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5BE5BAC6
P 6175 7450
F 0 "#FLG01" H 6175 7525 50  0001 C CNN
F 1 "PWR_FLAG" H 6175 7624 50  0000 C CNN
F 2 "" H 6175 7450 50  0001 C CNN
F 3 "~" H 6175 7450 50  0001 C CNN
	1    6175 7450
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5BE5BB38
P 6550 7450
F 0 "#FLG02" H 6550 7525 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 7624 50  0000 C CNN
F 2 "" H 6550 7450 50  0001 C CNN
F 3 "~" H 6550 7450 50  0001 C CNN
	1    6550 7450
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR03
U 1 1 5BE5BC45
P 6175 7450
F 0 "#PWR03" H 6175 7300 50  0001 C CNN
F 1 "+12V" H 6190 7623 50  0000 C CNN
F 2 "" H 6175 7450 50  0001 C CNN
F 3 "" H 6175 7450 50  0001 C CNN
	1    6175 7450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5BE5BE6A
P 6550 7450
F 0 "#PWR010" H 6550 7200 50  0001 C CNN
F 1 "GND" H 6650 7450 50  0000 C CNN
F 2 "" H 6550 7450 50  0001 C CNN
F 3 "" H 6550 7450 50  0001 C CNN
	1    6550 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5BD30348
P 4550 6775
F 0 "R6" V 4550 6775 50  0000 C CNN
F 1 "5.1k" V 4475 6775 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4480 6775 50  0001 C CNN
F 3 "~" H 4550 6775 50  0001 C CNN
	1    4550 6775
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5BD304C9
P 4525 7575
F 0 "R8" V 4525 7575 50  0000 C CNN
F 1 "5.1k" V 4450 7575 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4455 7575 50  0001 C CNN
F 3 "~" H 4525 7575 50  0001 C CNN
	1    4525 7575
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5BD30571
P 3975 6625
F 0 "R5" V 3975 6625 50  0000 C CNN
F 1 "1k" V 3900 6625 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3905 6625 50  0001 C CNN
F 3 "~" H 3975 6625 50  0001 C CNN
	1    3975 6625
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5BD30615
P 3975 7400
F 0 "R7" V 3975 7400 50  0000 C CNN
F 1 "1k" V 3900 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3905 7400 50  0001 C CNN
F 3 "~" H 3975 7400 50  0001 C CNN
	1    3975 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	4125 6625 4200 6625
Wire Wire Line
	4200 6625 4200 6775
Wire Wire Line
	4200 6775 4400 6775
Connection ~ 4200 6625
Wire Wire Line
	4200 6625 4225 6625
Wire Wire Line
	4700 6775 4825 6775
Wire Wire Line
	4825 6775 4825 6525
Wire Wire Line
	4675 7575 4850 7575
Wire Wire Line
	4850 7575 4850 7300
Wire Wire Line
	4125 7400 4225 7400
Wire Wire Line
	4225 7575 4375 7575
Wire Wire Line
	4225 7400 4225 7575
Connection ~ 4225 7400
Wire Wire Line
	4225 7400 4250 7400
Wire Wire Line
	3825 7400 3825 7500
Wire Wire Line
	3825 6625 3825 6725
Text Label 4225 6425 2    50   ~ 0
SenseB2-AmpB2
Text Label 4250 7200 2    50   ~ 0
SenseA2-AmpA2
Wire Wire Line
	4850 7300 4925 7300
Wire Wire Line
	4825 6525 4925 6525
Text Label 4925 6525 0    50   ~ 0
AmpB2-A3
Text Label 4925 7300 0    50   ~ 0
AmpA2-A2
$Comp
L Device:R R2
U 1 1 5BD931A7
P 2425 6775
F 0 "R2" V 2425 6775 50  0000 C CNN
F 1 "5.1k" V 2350 6775 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2355 6775 50  0001 C CNN
F 3 "~" H 2425 6775 50  0001 C CNN
	1    2425 6775
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5BD931AE
P 2400 7575
F 0 "R4" V 2400 7575 50  0000 C CNN
F 1 "5.1k" V 2325 7575 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2330 7575 50  0001 C CNN
F 3 "~" H 2400 7575 50  0001 C CNN
	1    2400 7575
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5BD931B5
P 1850 6625
F 0 "R1" V 1850 6625 50  0000 C CNN
F 1 "1k" V 1775 6625 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1780 6625 50  0001 C CNN
F 3 "~" H 1850 6625 50  0001 C CNN
	1    1850 6625
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5BD931BC
P 1850 7400
F 0 "R3" V 1850 7400 50  0000 C CNN
F 1 "1k" V 1775 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1780 7400 50  0001 C CNN
F 3 "~" H 1850 7400 50  0001 C CNN
	1    1850 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 6625 2075 6625
Wire Wire Line
	2075 6625 2075 6775
Wire Wire Line
	2075 6775 2275 6775
Connection ~ 2075 6625
Wire Wire Line
	2075 6625 2100 6625
Wire Wire Line
	2575 6775 2700 6775
Wire Wire Line
	2700 6775 2700 6525
Wire Wire Line
	2550 7575 2725 7575
Wire Wire Line
	2725 7575 2725 7300
Wire Wire Line
	2000 7400 2100 7400
Wire Wire Line
	2100 7575 2250 7575
Wire Wire Line
	2100 7400 2100 7575
Connection ~ 2100 7400
Wire Wire Line
	2100 7400 2125 7400
Wire Wire Line
	1700 7400 1700 7500
Wire Wire Line
	1700 6625 1700 6725
Text Label 2100 6425 2    50   ~ 0
SenseB1-AmpB1
Text Label 2125 7200 2    50   ~ 0
SenseA1-AmpA1
Wire Wire Line
	2725 7300 2800 7300
Wire Wire Line
	2700 6525 2800 6525
Text Label 2800 6525 0    50   ~ 0
AmpB1-A1
Text Label 2800 7300 0    50   ~ 0
AmpA1-A0
NoConn ~ 1850 1375
NoConn ~ 1850 1475
NoConn ~ 1850 1575
NoConn ~ 1850 1675
NoConn ~ 1850 1775
NoConn ~ 1850 1875
NoConn ~ 1850 1975
NoConn ~ 1850 2075
NoConn ~ 1850 2175
NoConn ~ 1850 2275
NoConn ~ 1850 2375
NoConn ~ 1850 2475
NoConn ~ 1850 3175
NoConn ~ 1850 3375
$Comp
L Amplifier_Operational:LM324 Amp1
U 1 1 5BD703A4
P 2425 7300
F 0 "Amp1" H 2425 7667 50  0000 C CNN
F 1 "LM324" H 2425 7576 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 2375 7400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 2475 7500 50  0001 C CNN
	1    2425 7300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 Amp1
U 2 1 5BD70549
P 2400 6525
F 0 "Amp1" H 2400 6892 50  0000 C CNN
F 1 "LM324" H 2400 6801 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 2350 6625 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 2450 6725 50  0001 C CNN
	2    2400 6525
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 Amp1
U 3 1 5BD7065C
P 4550 7300
F 0 "Amp1" H 4550 7667 50  0000 C CNN
F 1 "LM324" H 4550 7576 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 4500 7400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 4600 7500 50  0001 C CNN
	3    4550 7300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 Amp1
U 4 1 5BD70751
P 4525 6525
F 0 "Amp1" H 4525 6892 50  0000 C CNN
F 1 "LM324" H 4525 6801 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 4475 6625 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 4575 6725 50  0001 C CNN
	4    4525 6525
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 Amp1
U 5 1 5BD70858
P 1525 7050
F 0 "Amp1" H 1483 7096 50  0000 L CNN
F 1 "LM324" H 1483 7005 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 1475 7150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 1575 7250 50  0001 C CNN
	5    1525 7050
	1    0    0    -1  
$EndComp
Connection ~ 2700 6525
Connection ~ 2725 7300
Connection ~ 4850 7300
Connection ~ 4825 6525
Wire Wire Line
	8950 3800 8950 4250
Connection ~ 8950 4250
Wire Wire Line
	8950 1000 8950 1450
Connection ~ 8950 1450
Text Label 4450 5275 0    50   ~ 0
D49-Tr1
Text Label 4450 4975 0    50   ~ 0
D46-Tr4
NoConn ~ 4450 3375
NoConn ~ 4450 3475
NoConn ~ 4450 3575
NoConn ~ 4450 3675
NoConn ~ 4450 3775
NoConn ~ 4450 3875
NoConn ~ 4450 3975
NoConn ~ 4450 4075
Wire Wire Line
	6200 1000 6400 1000
Wire Wire Line
	6200 925  6200 1000
Connection ~ 6200 1000
Text Label 6200 925  0    50   ~ 0
5V4
Text Label 1850 5375 2    50   ~ 0
5V1
Wire Wire Line
	6200 3800 6200 3725
Connection ~ 6200 3800
Text Label 6200 3725 0    50   ~ 0
5V1
Text Label 1850 5475 2    50   ~ 0
5V3
Text Label 1850 5575 2    50   ~ 0
5V4
Text Label 1425 6750 2    50   ~ 0
5V4
$Comp
L Device:D_Zener D17
U 1 1 5BE3F052
P 1475 825
F 0 "D17" V 1429 904 50  0000 L CNN
F 1 "D_Zener" V 1520 904 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P2.54mm_Vertical_AnodeUp" H 1475 825 50  0001 C CNN
F 3 "~" H 1475 825 50  0001 C CNN
	1    1475 825 
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 5BE44583
P 1125 1125
F 0 "R17" H 1195 1171 50  0000 L CNN
F 1 "R" H 1195 1080 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1055 1125 50  0001 C CNN
F 3 "~" H 1125 1125 50  0001 C CNN
	1    1125 1125
	1    0    0    -1  
$EndComp
Wire Wire Line
	675  750  925  750 
Wire Wire Line
	925  750  925  675 
Wire Wire Line
	1325 675  1475 675 
Wire Wire Line
	1475 675  1625 675 
Connection ~ 1475 675 
Wire Wire Line
	1475 975  1125 975 
Wire Wire Line
	575  750  575  1275
Wire Wire Line
	575  1275 1125 1275
Connection ~ 1125 1275
$Comp
L Device:Q_PMOS_GDS Q1
U 1 1 5BE80064
P 1125 775
F 0 "Q1" V 1468 775 50  0000 C CNN
F 1 "Q_PMOS_GDS" V 1377 775 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 1325 875 50  0001 C CNN
F 3 "~" H 1125 775 50  0001 C CNN
	1    1125 775 
	0    -1   -1   0   
$EndComp
Connection ~ 1125 975 
Text Label 1850 3475 2    50   ~ 0
Ech1-A8
Text Label 1850 3575 2    50   ~ 0
Ech2-A9
Text Label 1850 3675 2    50   ~ 0
Ech3-A10
Text Label 1850 3775 2    50   ~ 0
Ech4-A11
Text Label 1850 3875 2    50   ~ 0
Ech5-A12
Text Label 1850 3975 2    50   ~ 0
Ech6-A13
Text Label 1850 4075 2    50   ~ 0
Ech7-A14
Text Label 1850 4175 2    50   ~ 0
Ech8-A15
Text Label 4450 4875 0    50   ~ 0
D45-Tr5
Text Label 4450 4775 0    50   ~ 0
D44-Tr6
Text Label 4450 4675 0    50   ~ 0
D43-Tr7
Text Label 4450 4575 0    50   ~ 0
D42-Tr8
Text Label 4450 2175 0    50   ~ 0
EncA1
Text Label 4450 2275 0    50   ~ 0
EncB1
Text Label 4450 2375 0    50   ~ 0
EncA2
Text Label 4450 2475 0    50   ~ 0
EncB2
Text Label 4450 5375 0    50   ~ 0
EncA3
Text Label 4450 5475 0    50   ~ 0
EncB3
Text Label 4450 5575 0    50   ~ 0
EncA4
Text Label 4450 5675 0    50   ~ 0
EncB4
Text Label 4450 2075 0    50   ~ 0
D9-EnB2
Text Label 4450 1975 0    50   ~ 0
D8-EnA2
Text Label 4450 1875 0    50   ~ 0
D7-EnB1
Text Label 4450 1775 0    50   ~ 0
D6-EnA1
NoConn ~ 4450 1375
NoConn ~ 4450 1475
NoConn ~ 4450 1575
NoConn ~ 4450 1675
NoConn ~ 4450 4175
NoConn ~ 4450 4275
NoConn ~ 4450 4375
NoConn ~ 4450 4475
$Comp
L Switch:SW_Push RESET1
U 1 1 5BE336F7
P 1650 4475
F 0 "RESET1" H 1650 4425 50  0000 C CNN
F 1 "SW_Push" H 1650 4575 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 1650 4675 50  0001 C CNN
F 3 "" H 1650 4675 50  0001 C CNN
	1    1650 4475
	1    0    0    -1  
$EndComp
Text Label 1450 4475 2    50   ~ 0
GND2
$Comp
L power:GNDPWR #PWR02
U 1 1 5BE98085
P 1625 1275
F 0 "#PWR02" H 1625 1075 50  0001 C CNN
F 1 "GNDPWR" H 1629 1121 50  0000 C CNN
F 2 "" H 1625 1225 50  0001 C CNN
F 3 "" H 1625 1225 50  0001 C CNN
	1    1625 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3300 6150 3300
$Comp
L power:GNDPWR #PWR011
U 1 1 5BE98344
P 6700 1300
F 0 "#PWR011" H 6700 1100 50  0001 C CNN
F 1 "GNDPWR" H 6704 1146 50  0000 C CNN
F 2 "" H 6700 1250 50  0001 C CNN
F 3 "" H 6700 1250 50  0001 C CNN
	1    6700 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR04
U 1 1 5BE98466
P 6150 3300
F 0 "#PWR04" H 6150 3100 50  0001 C CNN
F 1 "GNDPWR" H 6154 3146 50  0000 C CNN
F 2 "" H 6150 3250 50  0001 C CNN
F 3 "" H 6150 3250 50  0001 C CNN
	1    6150 3300
	1    0    0    -1  
$EndComp
Connection ~ 6150 3300
Wire Wire Line
	6150 3300 6200 3300
$Comp
L power:GNDPWR #PWR017
U 1 1 5BE984F9
P 8200 2900
F 0 "#PWR017" H 8200 2700 50  0001 C CNN
F 1 "GNDPWR" H 8204 2746 50  0000 C CNN
F 2 "" H 8200 2850 50  0001 C CNN
F 3 "" H 8200 2850 50  0001 C CNN
	1    8200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 6100 6150 6100
$Comp
L power:GNDPWR #PWR07
U 1 1 5BE98767
P 6150 6100
F 0 "#PWR07" H 6150 5900 50  0001 C CNN
F 1 "GNDPWR" H 6154 5946 50  0000 C CNN
F 2 "" H 6150 6050 50  0001 C CNN
F 3 "" H 6150 6050 50  0001 C CNN
	1    6150 6100
	1    0    0    -1  
$EndComp
Connection ~ 6150 6100
Wire Wire Line
	6150 6100 6200 6100
$Comp
L power:GNDPWR #PWR018
U 1 1 5BE98880
P 8200 5700
F 0 "#PWR018" H 8200 5500 50  0001 C CNN
F 1 "GNDPWR" H 8204 5546 50  0000 C CNN
F 2 "" H 8200 5650 50  0001 C CNN
F 3 "" H 8200 5650 50  0001 C CNN
	1    8200 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR012
U 1 1 5BE989A2
P 6700 4100
F 0 "#PWR012" H 6700 3900 50  0001 C CNN
F 1 "GNDPWR" H 6704 3946 50  0000 C CNN
F 2 "" H 6700 4050 50  0001 C CNN
F 3 "" H 6700 4050 50  0001 C CNN
	1    6700 4100
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5BE98D1D
P 5725 7450
F 0 "#FLG0101" H 5725 7525 50  0001 C CNN
F 1 "PWR_FLAG" H 5725 7624 50  0000 C CNN
F 2 "" H 5725 7450 50  0001 C CNN
F 3 "~" H 5725 7450 50  0001 C CNN
	1    5725 7450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0101
U 1 1 5BE98DAE
P 5725 7450
F 0 "#PWR0101" H 5725 7250 50  0001 C CNN
F 1 "GNDPWR" H 5729 7296 50  0000 C CNN
F 2 "" H 5725 7400 50  0001 C CNN
F 3 "" H 5725 7400 50  0001 C CNN
	1    5725 7450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 5BE990E6
P 6400 5550
F 0 "#PWR0102" H 6400 5350 50  0001 C CNN
F 1 "GNDPWR" H 6404 5396 50  0000 C CNN
F 2 "" H 6400 5500 50  0001 C CNN
F 3 "" H 6400 5500 50  0001 C CNN
	1    6400 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0103
U 1 1 5BE99177
P 6400 2750
F 0 "#PWR0103" H 6400 2550 50  0001 C CNN
F 1 "GNDPWR" H 6404 2596 50  0000 C CNN
F 2 "" H 6400 2700 50  0001 C CNN
F 3 "" H 6400 2700 50  0001 C CNN
	1    6400 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5BE98FEE
P 1050 7050
F 0 "C7" H 850 7100 50  0000 L CNN
F 1 "100n" H 750 7000 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 1088 6900 50  0001 C CNN
F 3 "~" H 1050 7050 50  0001 C CNN
	1    1050 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 6900 1425 6900
Wire Wire Line
	1425 6750 1425 6900
Wire Wire Line
	1425 7200 1050 7200
Wire Wire Line
	1425 7200 1425 7350
Text Label 1425 6750 2    50   ~ 0
5V4
$Comp
L power:GNDPWR #PWR0104
U 1 1 5BEB7198
P 9575 5925
F 0 "#PWR0104" H 9575 5725 50  0001 C CNN
F 1 "GNDPWR" H 9579 5771 50  0000 C CNN
F 2 "" H 9575 5875 50  0001 C CNN
F 3 "" H 9575 5875 50  0001 C CNN
	1    9575 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1000 8950 1000
Wire Wire Line
	6700 3800 8950 3800
$Comp
L arduino:Arduino_Mega2560_Shield XA1
U 1 1 5BD2238B
P 3150 3525
F 0 "XA1" H 3150 1145 60  0000 C CNN
F 1 "Arduino_Mega2560_Shield" H 3150 1039 60  0000 C CNN
F 2 "Arduino:Arduino_Mega2560_Shield_modified" V 3175 3825 60  0000 C CNN
F 3 "https://store.arduino.cc/arduino-mega-2560-rev3" H 3850 6275 60  0001 C CNN
	1    3150 3525
	1    0    0    -1  
$EndComp
Text Label 1850 4775 2    50   ~ 0
GND1
Text Label 1850 4975 2    50   ~ 0
GND3
Text Label 1850 5075 2    50   ~ 0
GND5
NoConn ~ 1850 5175
Text Label 1700 6725 2    50   ~ 0
GND1
Text Label 1700 7500 2    50   ~ 0
GND1
Text Label 3825 6725 2    50   ~ 0
GND1
Text Label 3825 7500 2    50   ~ 0
GND1
Text Label 6200 4100 2    50   ~ 0
GND2
Text Label 1425 7350 0    50   ~ 0
GND1
Wire Wire Line
	10675 5925 10675 6000
Wire Wire Line
	10500 5925 10500 6000
Wire Wire Line
	10150 5925 10150 6025
Text Label 10150 6025 3    50   ~ 0
GND1
Text Label 10500 6000 3    50   ~ 0
GND3
Text Label 10675 6000 3    50   ~ 0
GND5
$Comp
L power:GND #PWR0105
U 1 1 5BEB8B7B
P 10850 5950
F 0 "#PWR0105" H 10850 5700 50  0001 C CNN
F 1 "GND" H 10855 5777 50  0000 C CNN
F 2 "" H 10850 5950 50  0001 C CNN
F 3 "" H 10850 5950 50  0001 C CNN
	1    10850 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 5925 10850 5950
$Comp
L Device:R R18
U 1 1 5BEC59D8
P 2300 725
F 0 "R18" H 2370 771 50  0000 L CNN
F 1 "47k" H 2370 680 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2230 725 50  0001 C CNN
F 3 "~" H 2300 725 50  0001 C CNN
	1    2300 725 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5BEC5E20
P 2300 1025
F 0 "R19" H 2370 1071 50  0000 L CNN
F 1 "10k" H 2370 980 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2230 1025 50  0001 C CNN
F 3 "~" H 2300 1025 50  0001 C CNN
	1    2300 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	1625 675  2000 675 
Wire Wire Line
	2000 675  2000 575 
Wire Wire Line
	2000 575  2300 575 
Connection ~ 1625 675 
Wire Wire Line
	1850 1275 1850 1175
Wire Wire Line
	1850 1175 2300 1175
Wire Wire Line
	1125 1275 1625 1275
Connection ~ 1625 1275
Wire Wire Line
	1625 1275 1850 1275
Wire Wire Line
	2300 875  2550 875 
Connection ~ 2300 875 
Text Label 1850 3075 2    50   ~ 0
Battery-A4
Text Label 2550 875  0    50   ~ 0
Battery-A4
Text Label 6200 1300 2    50   ~ 0
GND1
Text Label 1850 4875 2    50   ~ 0
GND2
Connection ~ 10500 5925
Wire Wire Line
	10500 5925 10675 5925
Connection ~ 10675 5925
Wire Wire Line
	10675 5925 10850 5925
Connection ~ 10275 5925
Wire Wire Line
	10275 5925 10500 5925
Wire Wire Line
	10150 5925 10275 5925
Wire Wire Line
	10275 5925 10275 6025
Text Label 10275 6025 3    50   ~ 0
GND2
$Comp
L MyLib:Via V1
U 1 1 5C128259
P 9850 5925
F 0 "V1" V 9585 5925 50  0000 C CNN
F 1 "Via" V 9676 5925 50  0000 C CNN
F 2 "Arduino:Via" H 9850 5925 50  0001 C CNN
F 3 "" H 9850 5925 50  0001 C CNN
	1    9850 5925
	0    1    1    0   
$EndComp
Wire Wire Line
	9575 5925 9650 5925
Wire Wire Line
	10050 5925 10150 5925
Connection ~ 10150 5925
$EndSCHEMATC
