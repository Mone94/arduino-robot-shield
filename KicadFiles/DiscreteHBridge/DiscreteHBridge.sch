EESchema Schematic File Version 4
LIBS:DiscreteHBridge-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_Motor:L298HN Mtr12
U 1 1 5BD22F41
P 5925 1950
F 0 "Mtr12" H 5925 2050 50  0000 C CNN
F 1 "L298HN" H 5975 1950 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 5975 1300 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 6075 2200 50  0001 C CNN
	1    5925 1950
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:L298HN Mtr34
U 1 1 5BD37B78
P 5925 4825
F 0 "Mtr34" H 5925 4925 50  0000 C CNN
F 1 "L298HN" H 5975 4825 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 5975 4175 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 6075 5075 50  0001 C CNN
	1    5925 4825
	1    0    0    -1  
$EndComp
Text Label 5925 1250 0    50   ~ 0
VSS
Text Label 6025 1250 0    50   ~ 0
VS
Text Label 5325 1450 0    50   ~ 0
In11
Text Label 5325 1550 0    50   ~ 0
In21
Text Label 5325 1650 0    50   ~ 0
EnA1
Text Label 5325 1850 0    50   ~ 0
In31
Text Label 5325 1950 0    50   ~ 0
In41
Text Label 5325 2050 0    50   ~ 0
EnB1
Text Label 5625 2650 0    50   ~ 0
SenseA1
Text Label 5725 2650 0    50   ~ 0
SenseB1
Text Label 5925 2650 0    50   ~ 0
GND
Text Label 6525 1750 0    50   ~ 0
Out11
Text Label 6525 1850 0    50   ~ 0
Out21
Text Label 6525 2050 0    50   ~ 0
Out31
Text Label 6525 2150 0    50   ~ 0
Out41
Text Label 5925 5525 0    50   ~ 0
GND
Text Label 5725 5525 0    50   ~ 0
SenseB2
Text Label 5625 5525 0    50   ~ 0
SenseA2
Text Label 5325 4925 0    50   ~ 0
EnB2
Text Label 5325 4825 0    50   ~ 0
In42
Text Label 5325 4725 0    50   ~ 0
In32
Text Label 6525 5025 0    50   ~ 0
Out42
Text Label 6525 4925 0    50   ~ 0
Out32
Text Label 6525 4725 0    50   ~ 0
Out22
Text Label 6525 4625 0    50   ~ 0
Out12
Text Label 5325 4525 0    50   ~ 0
EnA2
Text Label 5325 4425 0    50   ~ 0
In22
Text Label 5325 4325 0    50   ~ 0
In12
Text Label 5925 4125 0    50   ~ 0
VSS
Text Label 6025 4125 0    50   ~ 0
VS
$Comp
L Device:Q_PMOS_GDS Q15
U 1 1 5C3465AB
P 9500 1500
F 0 "Q15" H 9706 1546 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 9706 1455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9700 1600 50  0001 C CNN
F 3 "~" H 9500 1500 50  0001 C CNN
	1    9500 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GDS Q19
U 1 1 5C346607
P 10650 1500
F 0 "Q19" H 10856 1546 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 10856 1455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 10850 1600 50  0001 C CNN
F 3 "~" H 10650 1500 50  0001 C CNN
	1    10650 1500
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GDS Q3
U 1 1 5C346723
P 7350 1500
F 0 "Q3" H 7556 1546 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 7556 1455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7550 1600 50  0001 C CNN
F 3 "~" H 7350 1500 50  0001 C CNN
	1    7350 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GDS Q7
U 1 1 5C34672A
P 8500 1500
F 0 "Q7" H 8706 1546 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 8706 1455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8700 1600 50  0001 C CNN
F 3 "~" H 8500 1500 50  0001 C CNN
	1    8500 1500
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q16
U 1 1 5C34681B
P 9500 2425
F 0 "Q16" H 9705 2471 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 9705 2380 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9700 2525 50  0001 C CNN
F 3 "~" H 9500 2425 50  0001 C CNN
	1    9500 2425
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q4
U 1 1 5C3468C2
P 7350 2425
F 0 "Q4" H 7555 2471 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7555 2380 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7550 2525 50  0001 C CNN
F 3 "~" H 7350 2425 50  0001 C CNN
	1    7350 2425
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q8
U 1 1 5C3468F6
P 8500 2425
F 0 "Q8" H 8706 2471 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 8706 2380 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8700 2525 50  0001 C CNN
F 3 "~" H 8500 2425 50  0001 C CNN
	1    8500 2425
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q20
U 1 1 5C3469B3
P 10650 2425
F 0 "Q20" H 10856 2471 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 10856 2380 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 10850 2525 50  0001 C CNN
F 3 "~" H 10650 2425 50  0001 C CNN
	1    10650 2425
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C346B55
P 7150 1350
F 0 "R1" H 7220 1396 50  0000 L CNN
F 1 "R" H 7220 1305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 7080 1350 50  0001 C CNN
F 3 "~" H 7150 1350 50  0001 C CNN
	1    7150 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5C346BF1
P 8700 1350
F 0 "R3" H 8770 1396 50  0000 L CNN
F 1 "R" H 8770 1305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 8630 1350 50  0001 C CNN
F 3 "~" H 8700 1350 50  0001 C CNN
	1    8700 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C346C5B
P 9300 1350
F 0 "R5" H 9370 1396 50  0000 L CNN
F 1 "R" H 9370 1305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 9230 1350 50  0001 C CNN
F 3 "~" H 9300 1350 50  0001 C CNN
	1    9300 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5C346C9D
P 10850 1350
F 0 "R7" H 10920 1396 50  0000 L CNN
F 1 "R" H 10920 1305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 10780 1350 50  0001 C CNN
F 3 "~" H 10850 1350 50  0001 C CNN
	1    10850 1350
	1    0    0    -1  
$EndComp
Connection ~ 9600 2625
Wire Wire Line
	9600 2625 10550 2625
Wire Wire Line
	7150 1200 7450 1200
Connection ~ 9300 1200
Wire Wire Line
	9300 1200 9600 1200
Wire Wire Line
	9600 1200 9600 1300
Connection ~ 9600 1200
Wire Wire Line
	9600 1200 10550 1200
Wire Wire Line
	8400 1300 8400 1200
Connection ~ 8400 1200
Wire Wire Line
	7450 1300 7450 1200
Connection ~ 7450 1200
Wire Wire Line
	7450 1200 8400 1200
$Comp
L Device:Q_NPN_BCE Q1
U 1 1 5C37A5F0
P 7000 1925
F 0 "Q1" H 7191 1971 50  0000 L CNN
F 1 "Q_NPN_BCE" H 7191 1880 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7200 2025 50  0001 C CNN
F 3 "~" H 7000 1925 50  0001 C CNN
	1    7000 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q23
U 1 1 5C37A6B0
P 11000 1925
F 0 "Q23" H 11190 1971 50  0000 L CNN
F 1 "Q_NPN_BCE" H 11190 1880 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 11200 2025 50  0001 C CNN
F 3 "~" H 11000 1925 50  0001 C CNN
	1    11000 1925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7450 1700 7450 1900
Wire Wire Line
	8400 1700 8400 1900
Wire Wire Line
	10550 1700 10550 1975
Wire Wire Line
	8400 1200 8700 1200
Connection ~ 8700 1200
Wire Wire Line
	8700 1200 9000 1200
Wire Wire Line
	7450 2625 8400 2625
Connection ~ 8400 2625
Wire Wire Line
	8400 2625 8750 2625
Connection ~ 7450 2625
$Comp
L Device:Q_NPN_BCE Q13
U 1 1 5C3AE504
P 9150 1825
F 0 "Q13" H 9341 1871 50  0000 L CNN
F 1 "Q_NPN_BCE" H 9341 1780 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9350 1925 50  0001 C CNN
F 3 "~" H 9150 1825 50  0001 C CNN
	1    9150 1825
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q11
U 1 1 5C3AE55C
P 8850 2050
F 0 "Q11" H 9040 2096 50  0000 L CNN
F 1 "Q_NPN_BCE" H 9040 2005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9050 2150 50  0001 C CNN
F 3 "~" H 8850 2050 50  0001 C CNN
	1    8850 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7100 2125 7100 2625
Wire Wire Line
	7100 2625 7450 2625
Wire Wire Line
	5925 2650 7100 2650
Wire Wire Line
	7100 2650 7100 2625
Connection ~ 7100 2625
Wire Wire Line
	8750 2250 8750 2625
Connection ~ 8750 2625
Wire Wire Line
	9250 2025 9250 2625
Wire Wire Line
	8750 2625 9250 2625
Connection ~ 9250 2625
Wire Wire Line
	9250 2625 9600 2625
Wire Wire Line
	10900 2125 10900 2625
Wire Wire Line
	10900 2625 10550 2625
Connection ~ 10550 2625
Wire Wire Line
	10850 1500 10900 1500
Wire Wire Line
	10900 1500 10900 1725
Connection ~ 10850 1500
Wire Wire Line
	9300 1500 9250 1500
Wire Wire Line
	9250 1500 9250 1625
Connection ~ 9300 1500
Wire Wire Line
	8700 1500 8750 1500
Wire Wire Line
	8750 1500 8750 1850
Connection ~ 8700 1500
Wire Wire Line
	7100 1725 7100 1500
Wire Wire Line
	7100 1500 7150 1500
Connection ~ 7150 1500
Wire Wire Line
	7450 1900 7600 1900
Connection ~ 7450 1900
Wire Wire Line
	7450 1900 7450 2225
Wire Wire Line
	8400 1900 8225 1900
Connection ~ 8400 1900
Wire Wire Line
	8400 1900 8400 2225
Wire Wire Line
	9600 1975 9750 1975
Wire Wire Line
	9600 1700 9600 1975
Connection ~ 9600 1975
Wire Wire Line
	9600 1975 9600 2225
Wire Wire Line
	10550 1975 10350 1975
Connection ~ 10550 1975
Wire Wire Line
	10550 1975 10550 2225
Text Label 7600 1900 0    50   ~ 0
Out11
Text Label 8225 1900 2    50   ~ 0
Out21
Text Label 9750 1975 0    50   ~ 0
Out31
Text Label 10350 1975 2    50   ~ 0
Out41
Text Label 6800 1925 2    50   ~ 0
In11
Text Label 8700 2425 0    50   ~ 0
In11
Text Label 7150 2425 2    50   ~ 0
In21
Text Label 9050 2050 0    50   ~ 0
In21
Text Label 8950 1825 2    50   ~ 0
In31
Text Label 10850 2425 0    50   ~ 0
In31
Text Label 9300 2425 2    50   ~ 0
In41
Text Label 11200 1925 0    50   ~ 0
In41
Wire Wire Line
	5925 1250 5925 1000
Wire Wire Line
	5925 1000 9000 1000
Wire Wire Line
	9000 1000 9000 1200
Connection ~ 9000 1200
Wire Wire Line
	9000 1200 9300 1200
$Comp
L Device:Q_PMOS_GDS Q17
U 1 1 5C3B7E17
P 9500 4375
F 0 "Q17" H 9706 4421 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 9706 4330 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9700 4475 50  0001 C CNN
F 3 "~" H 9500 4375 50  0001 C CNN
	1    9500 4375
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GDS Q21
U 1 1 5C3B7E1E
P 10650 4375
F 0 "Q21" H 10856 4421 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 10856 4330 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 10850 4475 50  0001 C CNN
F 3 "~" H 10650 4375 50  0001 C CNN
	1    10650 4375
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GDS Q5
U 1 1 5C3B7E25
P 7350 4375
F 0 "Q5" H 7556 4421 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 7556 4330 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7550 4475 50  0001 C CNN
F 3 "~" H 7350 4375 50  0001 C CNN
	1    7350 4375
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GDS Q9
U 1 1 5C3B7E2C
P 8500 4375
F 0 "Q9" H 8706 4421 50  0000 L CNN
F 1 "Q_PMOS_GDS" H 8706 4330 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8700 4475 50  0001 C CNN
F 3 "~" H 8500 4375 50  0001 C CNN
	1    8500 4375
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q18
U 1 1 5C3B7E33
P 9500 5300
F 0 "Q18" H 9705 5346 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 9705 5255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9700 5400 50  0001 C CNN
F 3 "~" H 9500 5300 50  0001 C CNN
	1    9500 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q6
U 1 1 5C3B7E3A
P 7350 5300
F 0 "Q6" H 7555 5346 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7555 5255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7550 5400 50  0001 C CNN
F 3 "~" H 7350 5300 50  0001 C CNN
	1    7350 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q10
U 1 1 5C3B7E41
P 8500 5300
F 0 "Q10" H 8706 5346 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 8706 5255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8700 5400 50  0001 C CNN
F 3 "~" H 8500 5300 50  0001 C CNN
	1    8500 5300
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q22
U 1 1 5C3B7E48
P 10650 5300
F 0 "Q22" H 10856 5346 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 10856 5255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 10850 5400 50  0001 C CNN
F 3 "~" H 10650 5300 50  0001 C CNN
	1    10650 5300
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C3B7E4F
P 7150 4225
F 0 "R2" H 7220 4271 50  0000 L CNN
F 1 "R" H 7220 4180 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 7080 4225 50  0001 C CNN
F 3 "~" H 7150 4225 50  0001 C CNN
	1    7150 4225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5C3B7E56
P 8700 4225
F 0 "R4" H 8770 4271 50  0000 L CNN
F 1 "R" H 8770 4180 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 8630 4225 50  0001 C CNN
F 3 "~" H 8700 4225 50  0001 C CNN
	1    8700 4225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5C3B7E5D
P 9300 4225
F 0 "R6" H 9370 4271 50  0000 L CNN
F 1 "R" H 9370 4180 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 9230 4225 50  0001 C CNN
F 3 "~" H 9300 4225 50  0001 C CNN
	1    9300 4225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5C3B7E64
P 10850 4225
F 0 "R8" H 10920 4271 50  0000 L CNN
F 1 "R" H 10920 4180 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 10780 4225 50  0001 C CNN
F 3 "~" H 10850 4225 50  0001 C CNN
	1    10850 4225
	1    0    0    -1  
$EndComp
Connection ~ 9600 5500
Wire Wire Line
	9600 5500 10550 5500
Wire Wire Line
	7150 4075 7450 4075
Connection ~ 9300 4075
Wire Wire Line
	9300 4075 9600 4075
Wire Wire Line
	9600 4075 9600 4175
Connection ~ 9600 4075
Wire Wire Line
	9600 4075 10550 4075
Wire Wire Line
	8400 4175 8400 4075
Connection ~ 8400 4075
Wire Wire Line
	7450 4175 7450 4075
Connection ~ 7450 4075
Wire Wire Line
	7450 4075 8400 4075
$Comp
L Device:Q_NPN_BCE Q2
U 1 1 5C3B7E78
P 7000 4800
F 0 "Q2" H 7191 4846 50  0000 L CNN
F 1 "Q_NPN_BCE" H 7191 4755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7200 4900 50  0001 C CNN
F 3 "~" H 7000 4800 50  0001 C CNN
	1    7000 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q24
U 1 1 5C3B7E7F
P 11000 4800
F 0 "Q24" H 11190 4846 50  0000 L CNN
F 1 "Q_NPN_BCE" H 11190 4755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 11200 4900 50  0001 C CNN
F 3 "~" H 11000 4800 50  0001 C CNN
	1    11000 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7450 4575 7450 4775
Wire Wire Line
	8400 4575 8400 4775
Wire Wire Line
	10550 4575 10550 4850
Wire Wire Line
	8400 4075 8700 4075
Connection ~ 8700 4075
Wire Wire Line
	8700 4075 9000 4075
Wire Wire Line
	7450 5500 8400 5500
Connection ~ 8400 5500
Wire Wire Line
	8400 5500 8750 5500
Connection ~ 7450 5500
$Comp
L Device:Q_NPN_BCE Q14
U 1 1 5C3B7E90
P 9150 4700
F 0 "Q14" H 9341 4746 50  0000 L CNN
F 1 "Q_NPN_BCE" H 9341 4655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9350 4800 50  0001 C CNN
F 3 "~" H 9150 4700 50  0001 C CNN
	1    9150 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q12
U 1 1 5C3B7E97
P 8850 4925
F 0 "Q12" H 9040 4971 50  0000 L CNN
F 1 "Q_NPN_BCE" H 9040 4880 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 9050 5025 50  0001 C CNN
F 3 "~" H 8850 4925 50  0001 C CNN
	1    8850 4925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7100 5000 7100 5500
Wire Wire Line
	7100 5500 7450 5500
Wire Wire Line
	5925 5525 7100 5525
Wire Wire Line
	7100 5525 7100 5500
Connection ~ 7100 5500
Wire Wire Line
	8750 5125 8750 5500
Connection ~ 8750 5500
Wire Wire Line
	9250 4900 9250 5500
Wire Wire Line
	8750 5500 9250 5500
Connection ~ 9250 5500
Wire Wire Line
	9250 5500 9600 5500
Wire Wire Line
	10900 5000 10900 5500
Wire Wire Line
	10900 5500 10550 5500
Connection ~ 10550 5500
Wire Wire Line
	10850 4375 10900 4375
Wire Wire Line
	10900 4375 10900 4600
Connection ~ 10850 4375
Wire Wire Line
	9300 4375 9250 4375
Wire Wire Line
	9250 4375 9250 4500
Connection ~ 9300 4375
Wire Wire Line
	8700 4375 8750 4375
Wire Wire Line
	8750 4375 8750 4725
Connection ~ 8700 4375
Wire Wire Line
	7100 4600 7100 4375
Wire Wire Line
	7100 4375 7150 4375
Connection ~ 7150 4375
Wire Wire Line
	7450 4775 7600 4775
Connection ~ 7450 4775
Wire Wire Line
	7450 4775 7450 5100
Wire Wire Line
	8400 4775 8225 4775
Connection ~ 8400 4775
Wire Wire Line
	8400 4775 8400 5100
Wire Wire Line
	9600 4850 9750 4850
Wire Wire Line
	9600 4575 9600 4850
Connection ~ 9600 4850
Wire Wire Line
	9600 4850 9600 5100
Wire Wire Line
	10550 4850 10350 4850
Connection ~ 10550 4850
Wire Wire Line
	10550 4850 10550 5100
Text Label 7600 4775 0    50   ~ 0
Out12
Text Label 8225 4775 2    50   ~ 0
Out22
Text Label 9750 4850 0    50   ~ 0
Out32
Text Label 10350 4850 2    50   ~ 0
Out42
Text Label 6800 4800 2    50   ~ 0
In12
Text Label 8700 5300 0    50   ~ 0
In12
Text Label 7150 5300 2    50   ~ 0
In22
Text Label 9050 4925 0    50   ~ 0
In22
Text Label 8950 4700 2    50   ~ 0
In32
Text Label 10850 5300 0    50   ~ 0
In32
Text Label 9300 5300 2    50   ~ 0
In42
Wire Wire Line
	5925 3875 9000 3875
Wire Wire Line
	9000 3875 9000 4075
Connection ~ 9000 4075
Wire Wire Line
	9000 4075 9300 4075
Wire Wire Line
	5925 3875 5925 4125
Text Label 11200 4800 0    50   ~ 0
In42
Wire Wire Line
	10550 1300 10550 1200
Connection ~ 10550 1200
Wire Wire Line
	10550 1200 10850 1200
Wire Wire Line
	10550 4175 10550 4075
Connection ~ 10550 4075
Wire Wire Line
	10550 4075 10850 4075
$EndSCHEMATC
