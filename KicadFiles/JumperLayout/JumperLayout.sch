EESchema Schematic File Version 4
LIBS:JumperLayout-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L JumperLayout-rescue:L298HN-Driver_Motor Mtr12
U 1 1 5BD22F41
P 6400 2050
F 0 "Mtr12" H 6400 2150 50  0000 C CNN
F 1 "L298HN" H 6450 2050 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 6450 1400 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 6550 2300 50  0001 C CNN
	1    6400 2050
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:C-Device C2
U 1 1 5BD2317D
P 6700 1150
F 0 "C2" H 6815 1196 50  0000 L CNN
F 1 "100n" H 6815 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6738 1000 50  0001 C CNN
F 3 "~" H 6700 1150 50  0001 C CNN
	1    6700 1150
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:C-Device C1
U 1 1 5BD23229
P 6200 1150
F 0 "C1" H 6000 1200 50  0000 L CNN
F 1 "100n" H 5900 1100 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6238 1000 50  0001 C CNN
F 3 "~" H 6200 1150 50  0001 C CNN
	1    6200 1150
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R10
U 1 1 5BD23435
P 6200 3100
F 0 "R10" V 6200 3025 50  0000 L CNN
F 1 "0.15" H 6250 3100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6130 3100 50  0001 C CNN
F 3 "~" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R9
U 1 1 5BD234C3
P 6100 3100
F 0 "R9" V 6100 3050 50  0000 L CNN
F 1 "0.15" H 5875 3100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6030 3100 50  0001 C CNN
F 3 "~" H 6100 3100 50  0001 C CNN
	1    6100 3100
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:D-Device D7
U 1 1 5BD23561
P 8950 1600
F 0 "D7" H 8950 1500 50  0000 C CNN
F 1 "D" H 9050 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 1600 50  0001 C CNN
F 3 "~" H 8950 1600 50  0001 C CNN
	1    8950 1600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D5
U 1 1 5BD235ED
P 8700 1600
F 0 "D5" H 8700 1500 50  0000 C CNN
F 1 "D" H 8800 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 1600 50  0001 C CNN
F 3 "~" H 8700 1600 50  0001 C CNN
	1    8700 1600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D3
U 1 1 5BD2361F
P 8450 1600
F 0 "D3" H 8450 1500 50  0000 C CNN
F 1 "D" H 8550 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 1600 50  0001 C CNN
F 3 "~" H 8450 1600 50  0001 C CNN
	1    8450 1600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D1
U 1 1 5BD23651
P 8200 1600
F 0 "D1" H 8200 1500 50  0000 C CNN
F 1 "D" H 8300 1500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 1600 50  0001 C CNN
F 3 "~" H 8200 1600 50  0001 C CNN
	1    8200 1600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D8
U 1 1 5BD23B43
P 8950 2600
F 0 "D8" H 8950 2500 50  0000 C CNN
F 1 "D" H 9050 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 2600 50  0001 C CNN
F 3 "~" H 8950 2600 50  0001 C CNN
	1    8950 2600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D6
U 1 1 5BD23B4A
P 8700 2600
F 0 "D6" H 8700 2500 50  0000 C CNN
F 1 "D" H 8800 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 2600 50  0001 C CNN
F 3 "~" H 8700 2600 50  0001 C CNN
	1    8700 2600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D4
U 1 1 5BD23B51
P 8450 2600
F 0 "D4" H 8450 2500 50  0000 C CNN
F 1 "D" H 8550 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 2600 50  0001 C CNN
F 3 "~" H 8450 2600 50  0001 C CNN
	1    8450 2600
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D2
U 1 1 5BD23B58
P 8200 2600
F 0 "D2" H 8200 2500 50  0000 C CNN
F 1 "D" H 8300 2500 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 2600 50  0001 C CNN
F 3 "~" H 8200 2600 50  0001 C CNN
	1    8200 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 2750 6100 2800
Wire Wire Line
	6200 2750 6200 2900
Wire Wire Line
	6100 3250 6100 3300
Wire Wire Line
	6200 3250 6200 3300
Wire Wire Line
	6100 2800 5800 2800
Connection ~ 6100 2800
Wire Wire Line
	6100 2800 6100 2950
Wire Wire Line
	6200 2900 5800 2900
Connection ~ 6200 2900
Wire Wire Line
	6200 2900 6200 2950
Wire Wire Line
	6500 1350 6500 1000
Wire Wire Line
	6500 1000 6700 1000
Wire Wire Line
	6400 1000 6400 1350
Wire Wire Line
	8200 2750 8450 2750
Wire Wire Line
	8700 2750 8950 2750
Wire Wire Line
	8200 2750 8200 2900
Wire Wire Line
	8450 2750 8700 2750
Wire Wire Line
	8200 1450 8450 1450
Wire Wire Line
	8450 1450 8700 1450
Wire Wire Line
	8700 1450 8950 1450
$Comp
L JumperLayout-rescue:Conn_01x06_Male-Connector Mtr3
U 1 1 5BD176E9
P 9400 1650
F 0 "Mtr3" H 9500 2100 50  0000 C CNN
F 1 "Conn_01x06_Male" H 9550 2000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9400 1650 50  0001 C CNN
F 3 "~" H 9400 1650 50  0001 C CNN
	1    9400 1650
	1    0    0    -1  
$EndComp
Text Label 9600 1450 0    50   ~ 0
EncB3
Text Label 9600 1550 0    50   ~ 0
EncA3
Text Label 9600 1850 0    50   ~ 0
OutA3
Text Label 9600 1950 0    50   ~ 0
OutB3
$Comp
L JumperLayout-rescue:Conn_01x06_Male-Connector Mtr4
U 1 1 5BD17A19
P 9450 2800
F 0 "Mtr4" H 9550 3250 50  0000 C CNN
F 1 "Conn_01x06_Male" H 9550 3150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9450 2800 50  0001 C CNN
F 3 "~" H 9450 2800 50  0001 C CNN
	1    9450 2800
	1    0    0    -1  
$EndComp
Text Label 9650 2600 0    50   ~ 0
EncB4
Text Label 9650 2700 0    50   ~ 0
EncA4
Text Label 9650 3000 0    50   ~ 0
OutA4
Text Label 9650 3100 0    50   ~ 0
OutB4
$Comp
L JumperLayout-rescue:Conn_01x06_Male-Connector Mtr2
U 1 1 5BD17BFC
P 9450 3850
F 0 "Mtr2" H 9550 4300 50  0000 C CNN
F 1 "Conn_01x06_Male" H 9600 4200 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9450 3850 50  0001 C CNN
F 3 "~" H 9450 3850 50  0001 C CNN
	1    9450 3850
	1    0    0    -1  
$EndComp
Text Label 9650 3650 0    50   ~ 0
EncB2
Text Label 9650 3750 0    50   ~ 0
EncA2
Text Label 9650 4050 0    50   ~ 0
OutA2
Text Label 9650 4150 0    50   ~ 0
OutB2
$Comp
L JumperLayout-rescue:Conn_01x06_Male-Connector Mtr1
U 1 1 5BD17DE0
P 9450 4850
F 0 "Mtr1" H 9550 5300 50  0000 C CNN
F 1 "Conn_01x06_Male" H 9600 5200 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9450 4850 50  0001 C CNN
F 3 "~" H 9450 4850 50  0001 C CNN
	1    9450 4850
	1    0    0    -1  
$EndComp
Text Label 9650 4650 0    50   ~ 0
EncB1
Text Label 9650 4750 0    50   ~ 0
EncA1
Text Label 9650 5050 0    50   ~ 0
OutA1
Text Label 9650 5150 0    50   ~ 0
OutB1
Connection ~ 8450 1950
Connection ~ 8200 1850
Connection ~ 8200 2750
Connection ~ 8450 2750
Connection ~ 8700 2750
Connection ~ 8700 2150
Connection ~ 8950 2250
$Comp
L JumperLayout-rescue:+12V-power #PWR024
U 1 1 5BD26E43
P 6700 1000
F 0 "#PWR024" H 6700 850 50  0001 C CNN
F 1 "+12V" H 6715 1173 50  0000 C CNN
F 2 "" H 6700 1000 50  0001 C CNN
F 3 "" H 6700 1000 50  0001 C CNN
	1    6700 1000
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Screw_Terminal_01x02-Connector J1
U 1 1 5BD26F0C
P 650 625
F 0 "J1" V 650 700 50  0000 L CNN
F 1 "Screw" V 750 475 50  0000 L CNN
F 2 "Arduino:TerminalBlock_RND_205-00232_1x02_P5.08mm_Horizontal_Drill1.3" H 650 625 50  0001 C CNN
F 3 "~" H 650 625 50  0001 C CNN
	1    650  625 
	0    1    -1   0   
$EndComp
Connection ~ 8450 1450
Connection ~ 8700 1450
Text Label 7000 2250 0    50   ~ 0
OutB4
Text Label 7000 2150 0    50   ~ 0
OutA4
Text Label 7000 1950 0    50   ~ 0
OutB3
Text Label 7000 1850 0    50   ~ 0
OutA3
Wire Wire Line
	7000 1850 7950 1850
Wire Wire Line
	7000 1950 7350 1950
Wire Wire Line
	8450 1750 8450 1950
Wire Wire Line
	8450 1950 8450 2450
Wire Wire Line
	8700 1750 8700 2150
Wire Wire Line
	8700 2150 8700 2450
Wire Wire Line
	8950 1750 8950 2250
Wire Wire Line
	8950 2250 8950 2450
Wire Wire Line
	8200 1850 8200 2450
Wire Wire Line
	8200 1750 8200 1850
$Comp
L JumperLayout-rescue:R-Device R13
U 1 1 5BD2FD5A
P 7350 1600
F 0 "R13" V 7350 1525 50  0000 L CNN
F 1 "1.1k" H 7125 1600 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 1600 50  0001 C CNN
F 3 "~" H 7350 1600 50  0001 C CNN
	1    7350 1600
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R14
U 1 1 5BD2FDDC
P 7350 2500
F 0 "R14" V 7350 2425 50  0000 L CNN
F 1 "1.1k" H 7125 2500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 2500 50  0001 C CNN
F 3 "~" H 7350 2500 50  0001 C CNN
	1    7350 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2150 8700 2150
Wire Wire Line
	7350 1750 7350 1950
Connection ~ 7350 1950
Wire Wire Line
	7350 1950 8450 1950
Wire Wire Line
	7950 1450 7950 1850
Connection ~ 7950 1850
Wire Wire Line
	7950 1850 8200 1850
Connection ~ 6700 1000
$Comp
L JumperLayout-rescue:L298HN-Driver_Motor Mtr34
U 1 1 5BD37B78
P 6400 4850
F 0 "Mtr34" H 6400 4950 50  0000 C CNN
F 1 "L298HN" H 6450 4850 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 6450 4200 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 6550 5100 50  0001 C CNN
	1    6400 4850
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:C-Device C5
U 1 1 5BD37B7F
P 6700 3950
F 0 "C5" H 6815 3996 50  0000 L CNN
F 1 "100n" H 6815 3905 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6738 3800 50  0001 C CNN
F 3 "~" H 6700 3950 50  0001 C CNN
	1    6700 3950
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:C-Device C4
U 1 1 5BD37B86
P 6200 3950
F 0 "C4" H 6000 4000 50  0000 L CNN
F 1 "100n" H 5900 3900 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 6238 3800 50  0001 C CNN
F 3 "~" H 6200 3950 50  0001 C CNN
	1    6200 3950
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R12
U 1 1 5BD37B8D
P 6200 5900
F 0 "R12" V 6200 5825 50  0000 L CNN
F 1 "0.15" H 6250 5900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6130 5900 50  0001 C CNN
F 3 "~" H 6200 5900 50  0001 C CNN
	1    6200 5900
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R11
U 1 1 5BD37B94
P 6100 5900
F 0 "R11" V 6100 5825 50  0000 L CNN
F 1 "0.15" H 5875 5900 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P20.32mm_Horizontal" V 6030 5900 50  0001 C CNN
F 3 "~" H 6100 5900 50  0001 C CNN
	1    6100 5900
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:D-Device D15
U 1 1 5BD37B9B
P 8950 4400
F 0 "D15" H 8950 4300 50  0000 C CNN
F 1 "D" H 9050 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 4400 50  0001 C CNN
F 3 "~" H 8950 4400 50  0001 C CNN
	1    8950 4400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D13
U 1 1 5BD37BA2
P 8700 4400
F 0 "D13" H 8700 4300 50  0000 C CNN
F 1 "D" H 8800 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 4400 50  0001 C CNN
F 3 "~" H 8700 4400 50  0001 C CNN
	1    8700 4400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D11
U 1 1 5BD37BA9
P 8450 4400
F 0 "D11" H 8450 4300 50  0000 C CNN
F 1 "D" H 8550 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 4400 50  0001 C CNN
F 3 "~" H 8450 4400 50  0001 C CNN
	1    8450 4400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D9
U 1 1 5BD37BB0
P 8200 4400
F 0 "D9" H 8200 4300 50  0000 C CNN
F 1 "D" H 8300 4300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 4400 50  0001 C CNN
F 3 "~" H 8200 4400 50  0001 C CNN
	1    8200 4400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D16
U 1 1 5BD37BB7
P 8950 5400
F 0 "D16" H 8950 5300 50  0000 C CNN
F 1 "D" H 9050 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8950 5400 50  0001 C CNN
F 3 "~" H 8950 5400 50  0001 C CNN
	1    8950 5400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D14
U 1 1 5BD37BBE
P 8700 5400
F 0 "D14" H 8700 5300 50  0000 C CNN
F 1 "D" H 8800 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8700 5400 50  0001 C CNN
F 3 "~" H 8700 5400 50  0001 C CNN
	1    8700 5400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D12
U 1 1 5BD37BC5
P 8450 5400
F 0 "D12" H 8450 5300 50  0000 C CNN
F 1 "D" H 8550 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8450 5400 50  0001 C CNN
F 3 "~" H 8450 5400 50  0001 C CNN
	1    8450 5400
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:D-Device D10
U 1 1 5BD37BCC
P 8200 5400
F 0 "D10" H 8200 5300 50  0000 C CNN
F 1 "D" H 8300 5300 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P2.54mm_Vertical_AnodeUp" H 8200 5400 50  0001 C CNN
F 3 "~" H 8200 5400 50  0001 C CNN
	1    8200 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 5550 6100 5600
Wire Wire Line
	6200 5550 6200 5700
Wire Wire Line
	6100 6050 6100 6100
Wire Wire Line
	6200 6050 6200 6100
Wire Wire Line
	6100 5600 5800 5600
Connection ~ 6100 5600
Wire Wire Line
	6100 5600 6100 5750
Wire Wire Line
	6200 5700 5800 5700
Connection ~ 6200 5700
Wire Wire Line
	6200 5700 6200 5750
Wire Wire Line
	6500 4150 6500 3800
Wire Wire Line
	6500 3800 6700 3800
Wire Wire Line
	6200 3800 6400 3800
Wire Wire Line
	6400 3800 6400 4150
Wire Wire Line
	8200 5550 8450 5550
Wire Wire Line
	8700 5550 8950 5550
Wire Wire Line
	8200 5550 8200 5700
Wire Wire Line
	8450 5550 8700 5550
Wire Wire Line
	8200 4250 8450 4250
Wire Wire Line
	8450 4250 8700 4250
Wire Wire Line
	8700 4250 8950 4250
Connection ~ 8450 4750
Connection ~ 8200 4650
Connection ~ 8200 5550
Connection ~ 8450 5550
Connection ~ 8700 5550
Connection ~ 8700 4950
Connection ~ 8950 5050
$Comp
L JumperLayout-rescue:+12V-power #PWR026
U 1 1 5BD37C21
P 6700 3800
F 0 "#PWR026" H 6700 3650 50  0001 C CNN
F 1 "+12V" H 6715 3973 50  0000 C CNN
F 2 "" H 6700 3800 50  0001 C CNN
F 3 "" H 6700 3800 50  0001 C CNN
	1    6700 3800
	1    0    0    -1  
$EndComp
Connection ~ 8450 4250
Connection ~ 8700 4250
Text Label 7000 5050 0    50   ~ 0
OutB1
Text Label 7000 4950 0    50   ~ 0
OutA1
Text Label 7000 4750 0    50   ~ 0
OutB2
Text Label 7000 4650 0    50   ~ 0
OutA2
Wire Wire Line
	7000 4650 7950 4650
Wire Wire Line
	7000 4750 7350 4750
Wire Wire Line
	7000 4950 7950 4950
Wire Wire Line
	7000 5050 7350 5050
Wire Wire Line
	8450 4550 8450 4750
Wire Wire Line
	8450 4750 8450 5250
Wire Wire Line
	8700 4550 8700 4950
Wire Wire Line
	8700 4950 8700 5250
Wire Wire Line
	8950 4550 8950 5050
Wire Wire Line
	8950 5050 8950 5250
Wire Wire Line
	8200 4650 8200 5250
Wire Wire Line
	8200 4550 8200 4650
$Comp
L JumperLayout-rescue:R-Device R15
U 1 1 5BD37C39
P 7350 4400
F 0 "R15" V 7350 4325 50  0000 L CNN
F 1 "1.1k" H 7125 4400 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 4400 50  0001 C CNN
F 3 "~" H 7350 4400 50  0001 C CNN
	1    7350 4400
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R16
U 1 1 5BD37C40
P 7350 5400
F 0 "R16" V 7350 5325 50  0000 L CNN
F 1 "1.1k" H 7125 5400 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7280 5400 50  0001 C CNN
F 3 "~" H 7350 5400 50  0001 C CNN
	1    7350 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 5250 7350 5050
Connection ~ 7350 5050
Wire Wire Line
	7350 5050 8950 5050
Wire Wire Line
	7950 5550 7950 4950
Connection ~ 7950 4950
Wire Wire Line
	7950 4950 8700 4950
Wire Wire Line
	7350 4550 7350 4750
Connection ~ 7350 4750
Wire Wire Line
	7350 4750 8450 4750
Wire Wire Line
	7950 4250 7950 4650
Connection ~ 7950 4650
Wire Wire Line
	7950 4650 8200 4650
Connection ~ 6700 3800
Text Label 5800 1750 2    50   ~ 0
D6-EnM3
Text Label 5800 2150 2    50   ~ 0
D7-EnM4
Text Label 5800 4950 2    50   ~ 0
D9-EnM1
Text Label 5800 4550 2    50   ~ 0
D8-EnM2
Text Label 4775 2625 0    50   ~ 0
D22-In31
Text Label 4775 2725 0    50   ~ 0
D23-In32
Text Label 4775 2825 0    50   ~ 0
D24-In41
Text Label 4775 2925 0    50   ~ 0
D25-In42
Text Label 5800 1550 2    50   ~ 0
D22-In31
Text Label 5800 1650 2    50   ~ 0
D23-In32
Text Label 5800 1950 2    50   ~ 0
D24-In41
Text Label 5800 2050 2    50   ~ 0
D25-In42
Text Label 5800 4350 2    50   ~ 0
D26-In21
Text Label 5800 4450 2    50   ~ 0
D27-In22
Text Label 5800 4750 2    50   ~ 0
D28-In11
Text Label 5800 4850 2    50   ~ 0
D29-In12
Text Label 5800 2800 2    50   ~ 0
SenseM3-AmpA
Text Label 5800 2900 2    50   ~ 0
SenseM4-AmpB
Text Label 5800 5600 2    50   ~ 0
SenseM2-AmpC
Text Label 5800 5700 2    50   ~ 0
SenseM1-AmpD
Text Label 2175 2725 2    50   ~ 0
AmpM3-A0
Text Label 2175 2825 2    50   ~ 0
AmpM4-A1
Text Label 4775 3025 0    50   ~ 0
D26-In21
Text Label 4775 3125 0    50   ~ 0
D27-In22
Text Label 4775 3225 0    50   ~ 0
D28-In11
Text Label 4775 3325 0    50   ~ 0
D29-In12
Text Label 2175 2925 2    50   ~ 0
AmpM2-A2
Text Label 2175 3025 2    50   ~ 0
AmpM1-A3
Text Label 9250 3350 0    50   ~ 0
Motor2
Text Label 9250 4350 0    50   ~ 0
Motor1
Text Label 9200 1150 0    50   ~ 0
Motor3
Text Label 9225 2300 0    50   ~ 0
Motor4
NoConn ~ 2175 3325
NoConn ~ 2175 4325
NoConn ~ 2175 4625
NoConn ~ 2175 5725
NoConn ~ 3225 975 
NoConn ~ 3325 975 
NoConn ~ 3425 975 
NoConn ~ 3525 975 
NoConn ~ 3625 975 
NoConn ~ 3725 975 
NoConn ~ 2175 5325
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US1
U 1 1 5BE4CD47
P 1150 2075
F 0 "US1" H 525 2250 50  0000 L CNN
F 1 "Ultrasonic1" V 1200 1825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 2075 50  0001 C CNN
F 3 "~" H 1150 2075 50  0001 C CNN
	1    1150 2075
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US2
U 1 1 5BE4D00E
P 1150 2575
F 0 "US2" H 500 2700 50  0000 L CNN
F 1 "Ultrasonic2" V 1200 2325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 2575 50  0001 C CNN
F 3 "~" H 1150 2575 50  0001 C CNN
	1    1150 2575
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US3
U 1 1 5BE4D0A0
P 1150 3075
F 0 "US3" H 500 3225 50  0000 L CNN
F 1 "Ultrasonic3" V 1200 2825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 3075 50  0001 C CNN
F 3 "~" H 1150 3075 50  0001 C CNN
	1    1150 3075
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US4
U 1 1 5BE4D13E
P 1150 3575
F 0 "US4" H 500 3700 50  0000 L CNN
F 1 "Ultrasonic4" V 1200 3325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 3575 50  0001 C CNN
F 3 "~" H 1150 3575 50  0001 C CNN
	1    1150 3575
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US5
U 1 1 5BE4D1C0
P 1150 4075
F 0 "US5" H 475 4200 50  0000 L CNN
F 1 "Ultrasonic5" V 1200 3825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 4075 50  0001 C CNN
F 3 "~" H 1150 4075 50  0001 C CNN
	1    1150 4075
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US6
U 1 1 5BE4D258
P 1150 4575
F 0 "US6" H 500 4725 50  0000 L CNN
F 1 "Ultrasonic6" V 1200 4325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 4575 50  0001 C CNN
F 3 "~" H 1150 4575 50  0001 C CNN
	1    1150 4575
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US7
U 1 1 5BE4D2E2
P 1150 5075
F 0 "US7" H 500 5200 50  0000 L CNN
F 1 "Ultrasonic7" V 1200 4825 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 5075 50  0001 C CNN
F 3 "~" H 1150 5075 50  0001 C CNN
	1    1150 5075
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Conn_01x04_Female-Connector US8
U 1 1 5BE4D36A
P 1150 5575
F 0 "US8" H 500 5725 50  0000 L CNN
F 1 "Ultrasonic8" V 1200 5325 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1150 5575 50  0001 C CNN
F 3 "~" H 1150 5575 50  0001 C CNN
	1    1150 5575
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:+12V-power #PWR01
U 1 1 5BD276DF
P 1700 725
F 0 "#PWR01" H 1700 575 50  0001 C CNN
F 1 "+12V" H 1715 898 50  0000 C CNN
F 2 "" H 1700 725 50  0001 C CNN
F 3 "" H 1700 725 50  0001 C CNN
	1    1700 725 
	1    0    0    -1  
$EndComp
Text Label 950  1975 2    50   ~ 0
5V3
Text Label 950  2475 2    50   ~ 0
5V3
Text Label 950  2975 2    50   ~ 0
5V3
Text Label 950  3475 2    50   ~ 0
5V3
Text Label 950  3975 2    50   ~ 0
5V3
Text Label 950  4475 2    50   ~ 0
5V3
Text Label 950  4975 2    50   ~ 0
5V3
Text Label 950  5475 2    50   ~ 0
5V3
Text Label 950  2275 2    50   ~ 0
GND5
Text Label 950  2775 2    50   ~ 0
GND5
Text Label 950  3275 2    50   ~ 0
GND5
Text Label 950  3775 2    50   ~ 0
GND5
Text Label 950  4275 2    50   ~ 0
GND5
Text Label 950  4775 2    50   ~ 0
GND5
Text Label 950  5275 2    50   ~ 0
GND5
Text Label 950  5775 2    50   ~ 0
GND5
Text Label 950  2075 2    50   ~ 0
D49-Tr1
Text Label 950  2575 2    50   ~ 0
D48-Tr2
Text Label 950  3075 2    50   ~ 0
D47-Tr3
Text Label 950  3575 2    50   ~ 0
D46-Tr4
Text Label 950  4075 2    50   ~ 0
D45-Tr5
Text Label 950  4575 2    50   ~ 0
D44-Tr6
Text Label 950  5075 2    50   ~ 0
D43-Tr7
Text Label 950  5575 2    50   ~ 0
D42-Tr8
Text Label 950  2175 2    50   ~ 0
Ech1-A8
Text Label 950  2675 2    50   ~ 0
Ech2-A9
Text Label 950  3175 2    50   ~ 0
Ech3-A10
Text Label 950  3675 2    50   ~ 0
Ech4-A11
Text Label 950  4175 2    50   ~ 0
Ech5-A12
Text Label 950  4675 2    50   ~ 0
Ech6-A13
Text Label 950  5175 2    50   ~ 0
Ech7-A14
Text Label 950  5675 2    50   ~ 0
Ech8-A15
Text Label 4775 5125 0    50   ~ 0
D47-Tr3
Text Label 4775 5225 0    50   ~ 0
D48-Tr2
$Comp
L JumperLayout-rescue:PWR_FLAG-power #FLG01
U 1 1 5BE5BAC6
P 6175 7450
F 0 "#FLG01" H 6175 7525 50  0001 C CNN
F 1 "PWR_FLAG" H 6175 7624 50  0000 C CNN
F 2 "" H 6175 7450 50  0001 C CNN
F 3 "~" H 6175 7450 50  0001 C CNN
	1    6175 7450
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:PWR_FLAG-power #FLG02
U 1 1 5BE5BB38
P 6550 7450
F 0 "#FLG02" H 6550 7525 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 7624 50  0000 C CNN
F 2 "" H 6550 7450 50  0001 C CNN
F 3 "~" H 6550 7450 50  0001 C CNN
	1    6550 7450
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:+12V-power #PWR03
U 1 1 5BE5BC45
P 6175 7450
F 0 "#PWR03" H 6175 7300 50  0001 C CNN
F 1 "+12V" H 6190 7623 50  0000 C CNN
F 2 "" H 6175 7450 50  0001 C CNN
F 3 "" H 6175 7450 50  0001 C CNN
	1    6175 7450
	-1   0    0    1   
$EndComp
$Comp
L JumperLayout-rescue:GND-power #PWR010
U 1 1 5BE5BE6A
P 6550 7450
F 0 "#PWR010" H 6550 7200 50  0001 C CNN
F 1 "GND" H 6650 7450 50  0000 C CNN
F 2 "" H 6550 7450 50  0001 C CNN
F 3 "" H 6550 7450 50  0001 C CNN
	1    6550 7450
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R6
U 1 1 5BD30348
P 4550 6775
F 0 "R6" V 4550 6775 50  0000 C CNN
F 1 "5.1k" V 4475 6775 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4480 6775 50  0001 C CNN
F 3 "~" H 4550 6775 50  0001 C CNN
	1    4550 6775
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R8
U 1 1 5BD304C9
P 4525 7575
F 0 "R8" V 4525 7575 50  0000 C CNN
F 1 "5.1k" V 4450 7575 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4455 7575 50  0001 C CNN
F 3 "~" H 4525 7575 50  0001 C CNN
	1    4525 7575
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R5
U 1 1 5BD30571
P 3975 6625
F 0 "R5" V 3975 6625 50  0000 C CNN
F 1 "1k" V 3900 6625 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3905 6625 50  0001 C CNN
F 3 "~" H 3975 6625 50  0001 C CNN
	1    3975 6625
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R7
U 1 1 5BD30615
P 3975 7400
F 0 "R7" V 3975 7400 50  0000 C CNN
F 1 "1k" V 3900 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3905 7400 50  0001 C CNN
F 3 "~" H 3975 7400 50  0001 C CNN
	1    3975 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	4125 6625 4200 6625
Wire Wire Line
	4200 6625 4200 6775
Wire Wire Line
	4200 6775 4400 6775
Connection ~ 4200 6625
Wire Wire Line
	4200 6625 4225 6625
Wire Wire Line
	4700 6775 4825 6775
Wire Wire Line
	4825 6775 4825 6525
Wire Wire Line
	4675 7575 4850 7575
Wire Wire Line
	4850 7575 4850 7300
Wire Wire Line
	4125 7400 4225 7400
Wire Wire Line
	4225 7575 4375 7575
Wire Wire Line
	4225 7400 4225 7575
Connection ~ 4225 7400
Wire Wire Line
	4225 7400 4250 7400
Wire Wire Line
	3825 7400 3825 7500
Wire Wire Line
	3825 6625 3825 6725
Text Label 4225 6425 2    50   ~ 0
SenseM1-AmpD
Text Label 4250 7200 2    50   ~ 0
SenseM2-AmpC
Wire Wire Line
	4850 7300 4925 7300
Wire Wire Line
	4825 6525 4925 6525
Text Label 4925 6525 0    50   ~ 0
AmpM1-A3
Text Label 4925 7300 0    50   ~ 0
AmpM2-A2
$Comp
L JumperLayout-rescue:R-Device R2
U 1 1 5BD931A7
P 2425 6775
F 0 "R2" V 2425 6775 50  0000 C CNN
F 1 "5.1k" V 2350 6775 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2355 6775 50  0001 C CNN
F 3 "~" H 2425 6775 50  0001 C CNN
	1    2425 6775
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R4
U 1 1 5BD931AE
P 2400 7575
F 0 "R4" V 2400 7575 50  0000 C CNN
F 1 "5.1k" V 2325 7575 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2330 7575 50  0001 C CNN
F 3 "~" H 2400 7575 50  0001 C CNN
	1    2400 7575
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R1
U 1 1 5BD931B5
P 1850 6625
F 0 "R1" V 1850 6625 50  0000 C CNN
F 1 "1k" V 1775 6625 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1780 6625 50  0001 C CNN
F 3 "~" H 1850 6625 50  0001 C CNN
	1    1850 6625
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R3
U 1 1 5BD931BC
P 1850 7400
F 0 "R3" V 1850 7400 50  0000 C CNN
F 1 "1k" V 1775 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1780 7400 50  0001 C CNN
F 3 "~" H 1850 7400 50  0001 C CNN
	1    1850 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 6625 2075 6625
Wire Wire Line
	2075 6625 2075 6775
Wire Wire Line
	2075 6775 2275 6775
Connection ~ 2075 6625
Wire Wire Line
	2075 6625 2100 6625
Wire Wire Line
	2575 6775 2700 6775
Wire Wire Line
	2700 6775 2700 6525
Wire Wire Line
	2550 7575 2725 7575
Wire Wire Line
	2725 7575 2725 7300
Wire Wire Line
	2000 7400 2100 7400
Wire Wire Line
	2100 7575 2250 7575
Wire Wire Line
	2100 7400 2100 7575
Connection ~ 2100 7400
Wire Wire Line
	2100 7400 2125 7400
Wire Wire Line
	1700 7400 1700 7500
Wire Wire Line
	1700 6625 1700 6725
Text Label 2100 6425 2    50   ~ 0
SenseM4-AmpB
Text Label 2125 7200 2    50   ~ 0
SenseM3-AmpA
Wire Wire Line
	2725 7300 2800 7300
Wire Wire Line
	2700 6525 2800 6525
Text Label 2800 6525 0    50   ~ 0
AmpM4-A1
Text Label 2800 7300 0    50   ~ 0
AmpM3-A0
NoConn ~ 2175 1425
NoConn ~ 2175 1525
NoConn ~ 2175 1625
NoConn ~ 2175 1725
NoConn ~ 2175 1825
NoConn ~ 2175 1925
NoConn ~ 2175 2025
NoConn ~ 2175 2125
NoConn ~ 2175 2225
NoConn ~ 2175 2325
NoConn ~ 2175 2425
NoConn ~ 2175 2525
NoConn ~ 2175 3225
NoConn ~ 2175 3425
$Comp
L JumperLayout-rescue:LM324-Amplifier_Operational Amp1
U 1 1 5BD703A4
P 2425 7300
F 0 "Amp1" H 2425 7667 50  0000 C CNN
F 1 "LM324" H 2425 7576 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 2375 7400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 2475 7500 50  0001 C CNN
	1    2425 7300
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LM324-Amplifier_Operational Amp1
U 2 1 5BD70549
P 2400 6525
F 0 "Amp1" H 2400 6892 50  0000 C CNN
F 1 "LM324" H 2400 6801 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 2350 6625 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 2450 6725 50  0001 C CNN
	2    2400 6525
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LM324-Amplifier_Operational Amp1
U 3 1 5BD7065C
P 4550 7300
F 0 "Amp1" H 4550 7667 50  0000 C CNN
F 1 "LM324" H 4550 7576 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 4500 7400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 4600 7500 50  0001 C CNN
	3    4550 7300
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LM324-Amplifier_Operational Amp1
U 4 1 5BD70751
P 4525 6525
F 0 "Amp1" H 4525 6892 50  0000 C CNN
F 1 "LM324" H 4525 6801 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 4475 6625 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 4575 6725 50  0001 C CNN
	4    4525 6525
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LM324-Amplifier_Operational Amp1
U 5 1 5BD70858
P 1175 7050
F 0 "Amp1" H 900 7600 50  0000 L CNN
F 1 "LM324" H 900 7525 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 1125 7150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 1225 7250 50  0001 C CNN
	5    1175 7050
	1    0    0    -1  
$EndComp
Connection ~ 2700 6525
Connection ~ 2725 7300
Connection ~ 4850 7300
Connection ~ 4825 6525
Wire Wire Line
	8950 3800 8950 4250
Connection ~ 8950 4250
Wire Wire Line
	8950 1000 8950 1450
Connection ~ 8950 1450
Text Label 4775 5325 0    50   ~ 0
D49-Tr1
Text Label 4775 5025 0    50   ~ 0
D46-Tr4
NoConn ~ 4775 3425
NoConn ~ 4775 3525
NoConn ~ 4775 3625
NoConn ~ 4775 3725
NoConn ~ 4775 3825
NoConn ~ 4775 3925
NoConn ~ 4775 4025
NoConn ~ 4775 4125
Wire Wire Line
	6200 1000 6400 1000
Wire Wire Line
	6200 925  6200 1000
Connection ~ 6200 1000
Text Label 6200 925  0    50   ~ 0
5V4
Text Label 2175 5425 2    50   ~ 0
5V1
Wire Wire Line
	6200 3800 6200 3725
Connection ~ 6200 3800
Text Label 6200 3725 0    50   ~ 0
5V1
Text Label 2175 5525 2    50   ~ 0
5V3
Text Label 2175 5625 2    50   ~ 0
5V4
Text Label 1075 6750 2    50   ~ 0
5V4
$Comp
L JumperLayout-rescue:D_Zener-Device D17
U 1 1 5BE3F052
P 1375 975
F 0 "D17" H 1300 1075 50  0000 L CNN
F 1 "D_Zener" V 1475 900 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P2.54mm_Vertical_AnodeUp" H 1375 975 50  0001 C CNN
F 3 "~" H 1375 975 50  0001 C CNN
	1    1375 975 
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:R-Device R17
U 1 1 5BE44583
P 1150 1275
F 0 "R17" V 1150 1200 50  0000 L CNN
F 1 "10k" H 950 1275 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1080 1275 50  0001 C CNN
F 3 "~" H 1150 1275 50  0001 C CNN
	1    1150 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	550  825  550  1425
Wire Wire Line
	550  1425 1150 1425
$Comp
L JumperLayout-rescue:Q_PMOS_GDS-Device Q1
U 1 1 5BE80064
P 1150 925
F 0 "Q1" V 1493 925 50  0000 C CNN
F 1 "Q_PMOS_GDS" V 1402 925 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 1350 1025 50  0001 C CNN
F 3 "~" H 1150 925 50  0001 C CNN
	1    1150 925 
	0    -1   -1   0   
$EndComp
Text Label 2175 3525 2    50   ~ 0
Ech1-A8
Text Label 2175 3625 2    50   ~ 0
Ech2-A9
Text Label 2175 3725 2    50   ~ 0
Ech3-A10
Text Label 2175 3825 2    50   ~ 0
Ech4-A11
Text Label 2175 3925 2    50   ~ 0
Ech5-A12
Text Label 2175 4025 2    50   ~ 0
Ech6-A13
Text Label 2175 4125 2    50   ~ 0
Ech7-A14
Text Label 2175 4225 2    50   ~ 0
Ech8-A15
Text Label 4775 4925 0    50   ~ 0
D45-Tr5
Text Label 4775 4825 0    50   ~ 0
D44-Tr6
Text Label 4775 4725 0    50   ~ 0
D43-Tr7
Text Label 4775 4625 0    50   ~ 0
D42-Tr8
Text Label 4775 2225 0    50   ~ 0
EncA3
Text Label 4775 2325 0    50   ~ 0
EncB3
Text Label 4775 2425 0    50   ~ 0
EncA4
Text Label 4775 2525 0    50   ~ 0
EncB4
Text Label 4775 5425 0    50   ~ 0
EncA2
Text Label 4775 5525 0    50   ~ 0
EncB2
Text Label 4775 5625 0    50   ~ 0
EncA1
Text Label 4775 5725 0    50   ~ 0
EncB1
Text Label 4775 2125 0    50   ~ 0
D9-EnM1
Text Label 4775 2025 0    50   ~ 0
D8-EnM2
Text Label 4775 1925 0    50   ~ 0
D7-EnM4
Text Label 4775 1825 0    50   ~ 0
D6-EnM3
NoConn ~ 4775 1425
NoConn ~ 4775 1525
NoConn ~ 4775 1625
NoConn ~ 4775 1725
NoConn ~ 4775 4225
NoConn ~ 4775 4325
NoConn ~ 4775 4425
NoConn ~ 4775 4525
$Comp
L JumperLayout-rescue:SW_Push-Switch RESET1
U 1 1 5BE336F7
P 1975 4525
F 0 "RESET1" H 1975 4475 50  0000 C CNN
F 1 "SW_Push" H 1975 4625 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 1975 4725 50  0001 C CNN
F 3 "" H 1975 4725 50  0001 C CNN
	1    1975 4525
	1    0    0    -1  
$EndComp
Text Label 1775 4525 3    50   ~ 0
GND2
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR02
U 1 1 5BE98085
P 1150 1425
F 0 "#PWR02" H 1150 1225 50  0001 C CNN
F 1 "GNDPWR" H 1154 1271 50  0000 C CNN
F 2 "" H 1150 1375 50  0001 C CNN
F 3 "" H 1150 1375 50  0001 C CNN
	1    1150 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3300 6150 3300
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR011
U 1 1 5BE98344
P 6700 1300
F 0 "#PWR011" H 6700 1100 50  0001 C CNN
F 1 "GNDPWR" H 6704 1146 50  0000 C CNN
F 2 "" H 6700 1250 50  0001 C CNN
F 3 "" H 6700 1250 50  0001 C CNN
	1    6700 1300
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR04
U 1 1 5BE98466
P 6150 3300
F 0 "#PWR04" H 6150 3100 50  0001 C CNN
F 1 "GNDPWR" H 6154 3146 50  0000 C CNN
F 2 "" H 6150 3250 50  0001 C CNN
F 3 "" H 6150 3250 50  0001 C CNN
	1    6150 3300
	1    0    0    -1  
$EndComp
Connection ~ 6150 3300
Wire Wire Line
	6150 3300 6200 3300
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR017
U 1 1 5BE984F9
P 8200 2900
F 0 "#PWR017" H 8200 2700 50  0001 C CNN
F 1 "GNDPWR" H 8204 2746 50  0000 C CNN
F 2 "" H 8200 2850 50  0001 C CNN
F 3 "" H 8200 2850 50  0001 C CNN
	1    8200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 6100 6150 6100
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR07
U 1 1 5BE98767
P 6150 6100
F 0 "#PWR07" H 6150 5900 50  0001 C CNN
F 1 "GNDPWR" H 6154 5946 50  0000 C CNN
F 2 "" H 6150 6050 50  0001 C CNN
F 3 "" H 6150 6050 50  0001 C CNN
	1    6150 6100
	1    0    0    -1  
$EndComp
Connection ~ 6150 6100
Wire Wire Line
	6150 6100 6200 6100
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR018
U 1 1 5BE98880
P 8200 5700
F 0 "#PWR018" H 8200 5500 50  0001 C CNN
F 1 "GNDPWR" H 8204 5546 50  0000 C CNN
F 2 "" H 8200 5650 50  0001 C CNN
F 3 "" H 8200 5650 50  0001 C CNN
	1    8200 5700
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR012
U 1 1 5BE989A2
P 6700 4100
F 0 "#PWR012" H 6700 3900 50  0001 C CNN
F 1 "GNDPWR" H 6704 3946 50  0000 C CNN
F 2 "" H 6700 4050 50  0001 C CNN
F 3 "" H 6700 4050 50  0001 C CNN
	1    6700 4100
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:PWR_FLAG-power #FLG0101
U 1 1 5BE98D1D
P 6175 6975
F 0 "#FLG0101" H 6175 7050 50  0001 C CNN
F 1 "PWR_FLAG" H 6175 7149 50  0000 C CNN
F 2 "" H 6175 6975 50  0001 C CNN
F 3 "~" H 6175 6975 50  0001 C CNN
	1    6175 6975
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR0101
U 1 1 5BE98DAE
P 6175 6975
F 0 "#PWR0101" H 6175 6775 50  0001 C CNN
F 1 "GNDPWR" H 6179 6821 50  0000 C CNN
F 2 "" H 6175 6925 50  0001 C CNN
F 3 "" H 6175 6925 50  0001 C CNN
	1    6175 6975
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR0102
U 1 1 5BE990E6
P 6400 5550
F 0 "#PWR0102" H 6400 5350 50  0001 C CNN
F 1 "GNDPWR" H 6404 5396 50  0000 C CNN
F 2 "" H 6400 5500 50  0001 C CNN
F 3 "" H 6400 5500 50  0001 C CNN
	1    6400 5550
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR0103
U 1 1 5BE99177
P 6400 2750
F 0 "#PWR0103" H 6400 2550 50  0001 C CNN
F 1 "GNDPWR" H 6404 2596 50  0000 C CNN
F 2 "" H 6400 2700 50  0001 C CNN
F 3 "" H 6400 2700 50  0001 C CNN
	1    6400 2750
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:C-Device C7
U 1 1 5BE98FEE
P 1075 7050
F 0 "C7" H 875 7100 50  0000 L CNN
F 1 "100n" H 775 7000 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 1113 6900 50  0001 C CNN
F 3 "~" H 1075 7050 50  0001 C CNN
	1    1075 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1075 6750 1075 6900
Wire Wire Line
	1075 7200 1075 7350
Text Label 1075 6750 2    50   ~ 0
5V4
$Comp
L JumperLayout-rescue:GNDPWR-power #PWR0104
U 1 1 5BEB7198
P 9575 5925
F 0 "#PWR0104" H 9575 5725 50  0001 C CNN
F 1 "GNDPWR" H 9579 5771 50  0000 C CNN
F 2 "" H 9575 5875 50  0001 C CNN
F 3 "" H 9575 5875 50  0001 C CNN
	1    9575 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1000 8950 1000
Wire Wire Line
	6700 3800 8950 3800
$Comp
L JumperLayout-rescue:Arduino_Mega2560_Shield-arduino XA1
U 1 1 5BD2238B
P 3475 3575
F 0 "XA1" H 3475 1195 60  0000 C CNN
F 1 "Arduino_Mega2560_Shield" H 3475 1089 60  0000 C CNN
F 2 "Arduino:Arduino_Mega2560_Shield_modified" V 3500 3875 60  0000 C CNN
F 3 "https://store.arduino.cc/arduino-mega-2560-rev3" H 4175 6325 60  0001 C CNN
	1    3475 3575
	1    0    0    -1  
$EndComp
Text Label 2175 4825 2    50   ~ 0
GND1
Text Label 2175 5025 2    50   ~ 0
GND3
Text Label 2175 5125 2    50   ~ 0
GND5
Text Label 1700 6725 2    50   ~ 0
GND6
Text Label 1700 7500 2    50   ~ 0
GND6
Text Label 3825 6725 2    50   ~ 0
GND6
Text Label 3825 7500 2    50   ~ 0
GND6
Text Label 6200 4100 2    50   ~ 0
GND2
Text Label 1075 7350 0    50   ~ 0
GND6
Wire Wire Line
	10675 5925 10675 6000
Wire Wire Line
	10500 5925 10500 6000
Text Label 10150 6025 3    50   ~ 0
GND1
Text Label 10500 6000 3    50   ~ 0
GND3
Text Label 10675 6000 3    50   ~ 0
GND5
$Comp
L JumperLayout-rescue:GND-power #PWR0105
U 1 1 5BEB8B7B
P 10850 5950
F 0 "#PWR0105" H 10850 5700 50  0001 C CNN
F 1 "GND" H 10855 5777 50  0000 C CNN
F 2 "" H 10850 5950 50  0001 C CNN
F 3 "" H 10850 5950 50  0001 C CNN
	1    10850 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 5925 10850 5950
$Comp
L JumperLayout-rescue:R-Device R18
U 1 1 5BEC59D8
P 1700 975
F 0 "R18" V 1700 900 50  0000 L CNN
F 1 "47k" H 1750 975 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1630 975 50  0001 C CNN
F 3 "~" H 1700 975 50  0001 C CNN
	1    1700 975 
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:R-Device R19
U 1 1 5BEC5E20
P 1700 1275
F 0 "R19" V 1700 1200 50  0000 L CNN
F 1 "10k" H 1750 1275 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1630 1275 50  0001 C CNN
F 3 "~" H 1700 1275 50  0001 C CNN
	1    1700 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1125 1850 1125
Text Label 2175 3125 2    50   ~ 0
Battery-A4
Text Label 1850 1125 0    50   ~ 0
Battery-A4
Text Label 6200 1300 2    50   ~ 0
GND1
Text Label 2175 4925 2    50   ~ 0
GND2
Wire Wire Line
	10275 5925 10275 6025
Text Label 10275 6025 3    50   ~ 0
GND2
Wire Wire Line
	9575 5925 9650 5925
$Comp
L JumperLayout-rescue:Jumper-Device JP2
U 1 1 5C2F8E37
P 9900 1750
F 0 "JP2" H 9900 1750 50  0000 C CNN
F 1 "Jumper" H 9900 1923 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9900 1750 50  0001 C CNN
F 3 "~" H 9900 1750 50  0001 C CNN
	1    9900 1750
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper-Device JP1
U 1 1 5C2F8EE7
P 9900 1650
F 0 "JP1" H 9900 1675 50  0000 C CNN
F 1 "Jumper" H 9900 1823 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9900 1650 50  0001 C CNN
F 3 "~" H 9900 1650 50  0001 C CNN
	1    9900 1650
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper_2_Open-Jumper JP3
U 1 1 5C324431
P 10500 1600
F 0 "JP3" V 10454 1698 50  0000 L CNN
F 1 "Jumper_2_Open" V 10545 1698 50  0000 L CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_2Jumper" H 10500 1600 50  0001 C CNN
F 3 "~" H 10500 1600 50  0001 C CNN
	1    10500 1600
	0    1    1    0   
$EndComp
Text Label 10200 1650 0    50   ~ 0
5V4
Text Label 10200 1750 0    50   ~ 0
GND1
Text Label 10500 1400 1    50   ~ 0
5V4
Text Label 10500 1800 3    50   ~ 0
GND1
$Comp
L JumperLayout-rescue:Jumper-Device JP5
U 1 1 5C3749F6
P 9950 2900
F 0 "JP5" H 9950 2900 50  0000 C CNN
F 1 "Jumper" H 9950 3073 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9950 2900 50  0001 C CNN
F 3 "~" H 9950 2900 50  0001 C CNN
	1    9950 2900
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper-Device JP4
U 1 1 5C3749FD
P 9950 2800
F 0 "JP4" H 9950 2825 50  0000 C CNN
F 1 "Jumper" H 9950 2973 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9950 2800 50  0001 C CNN
F 3 "~" H 9950 2800 50  0001 C CNN
	1    9950 2800
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper_2_Open-Jumper JP10
U 1 1 5C374A04
P 10550 2750
F 0 "JP10" V 10504 2848 50  0000 L CNN
F 1 "Jumper_2_Open" V 10595 2848 50  0000 L CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_2Jumper" H 10550 2750 50  0001 C CNN
F 3 "~" H 10550 2750 50  0001 C CNN
	1    10550 2750
	0    1    1    0   
$EndComp
Text Label 10250 2800 0    50   ~ 0
5V4
Text Label 10250 2900 0    50   ~ 0
GND1
Text Label 10550 2550 1    50   ~ 0
5V4
Text Label 10550 2950 3    50   ~ 0
GND1
$Comp
L JumperLayout-rescue:Jumper-Device JP7
U 1 1 5C388D06
P 9950 3950
F 0 "JP7" H 9950 3950 50  0000 C CNN
F 1 "Jumper" H 9950 4123 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9950 3950 50  0001 C CNN
F 3 "~" H 9950 3950 50  0001 C CNN
	1    9950 3950
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper-Device JP6
U 1 1 5C388D0D
P 9950 3850
F 0 "JP6" H 9950 3875 50  0000 C CNN
F 1 "Jumper" H 9950 4023 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9950 3850 50  0001 C CNN
F 3 "~" H 9950 3850 50  0001 C CNN
	1    9950 3850
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper_2_Open-Jumper JP11
U 1 1 5C388D14
P 10550 3800
F 0 "JP11" V 10504 3898 50  0000 L CNN
F 1 "Jumper_2_Open" V 10595 3898 50  0000 L CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_2Jumper" H 10550 3800 50  0001 C CNN
F 3 "~" H 10550 3800 50  0001 C CNN
	1    10550 3800
	0    1    1    0   
$EndComp
Text Label 10250 3850 0    50   ~ 0
5V1
Text Label 10250 3950 0    50   ~ 0
GND2
Text Label 10550 3600 1    50   ~ 0
5V1
Text Label 10550 4000 3    50   ~ 0
GND2
$Comp
L JumperLayout-rescue:Jumper-Device JP9
U 1 1 5C38FA91
P 9950 4950
F 0 "JP9" H 9950 4950 50  0000 C CNN
F 1 "Jumper" H 9950 5123 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9950 4950 50  0001 C CNN
F 3 "~" H 9950 4950 50  0001 C CNN
	1    9950 4950
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper-Device JP8
U 1 1 5C38FA98
P 9950 4850
F 0 "JP8" H 9950 4875 50  0000 C CNN
F 1 "Jumper" H 9950 5023 50  0001 C CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_Vertical" H 9950 4850 50  0001 C CNN
F 3 "~" H 9950 4850 50  0001 C CNN
	1    9950 4850
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Jumper_2_Open-Jumper JP12
U 1 1 5C38FA9F
P 10550 4800
F 0 "JP12" V 10504 4898 50  0000 L CNN
F 1 "Jumper_2_Open" V 10595 4898 50  0000 L CNN
F 2 "Arduino:PinHeader_1x02_P2.00mm_2Jumper" H 10550 4800 50  0001 C CNN
F 3 "~" H 10550 4800 50  0001 C CNN
	1    10550 4800
	0    1    1    0   
$EndComp
Text Label 10250 4850 0    50   ~ 0
5V1
Text Label 10250 4950 0    50   ~ 0
GND3
Text Label 10550 4600 1    50   ~ 0
5V1
Text Label 10550 5000 3    50   ~ 0
GND3
Text Label 2175 5225 2    50   ~ 0
GND6
Text Label 10350 6025 3    50   ~ 0
GND6
Wire Wire Line
	10150 5925 10150 6025
Wire Wire Line
	10350 5925 10350 6025
Wire Wire Line
	10150 5925 10275 5925
Connection ~ 10275 5925
Wire Wire Line
	10275 5925 10350 5925
Connection ~ 10350 5925
Wire Wire Line
	10350 5925 10500 5925
Connection ~ 10500 5925
Wire Wire Line
	10500 5925 10675 5925
Connection ~ 10675 5925
Wire Wire Line
	10675 5925 10850 5925
$Comp
L JumperLayout-rescue:Via-MyLib V1
U 1 1 5C34F2CF
P 9850 5925
F 0 "V1" V 9585 5925 50  0000 C CNN
F 1 "Via" V 9676 5925 50  0000 C CNN
F 2 "Arduino:Via" H 9850 5925 50  0001 C CNN
F 3 "" H 9850 5925 50  0001 C CNN
	1    9850 5925
	0    1    1    0   
$EndComp
Wire Wire Line
	10050 5925 10150 5925
Wire Wire Line
	10150 5925 10150 5930
Connection ~ 10150 5925
Wire Wire Line
	7950 4655 7950 4650
Wire Wire Line
	7000 2250 7350 2250
Wire Wire Line
	7950 2150 7000 2150
Connection ~ 7950 2150
Connection ~ 7350 2250
Wire Wire Line
	7350 2250 8950 2250
Wire Wire Line
	7950 2150 7950 2750
Wire Wire Line
	7350 2350 7350 2250
Wire Wire Line
	7350 2650 7350 2750
$Comp
L JumperLayout-rescue:LED_Dual_2pin-Device M2
U 1 1 5C39A603
P 7650 4250
F 0 "M2" H 7650 4250 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 4555 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 4250 50  0001 C CNN
F 3 "~" H 7650 4250 50  0001 C CNN
	1    7650 4250
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LED_Dual_2pin-Device M1
U 1 1 5C39A807
P 7650 5550
F 0 "M1" H 7650 5550 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 5325 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 5550 50  0001 C CNN
F 3 "~" H 7650 5550 50  0001 C CNN
	1    7650 5550
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LED_Dual_2pin-Device M4
U 1 1 5C39A8C1
P 7650 2750
F 0 "M4" H 7650 2750 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 2525 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 2750 50  0001 C CNN
F 3 "~" H 7650 2750 50  0001 C CNN
	1    7650 2750
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:LED_Dual_2pin-Device M3
U 1 1 5C39AAD7
P 7650 1450
F 0 "M3" H 7650 1450 50  0000 C CNN
F 1 "LED_Dual_2pin" H 7650 1750 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7650 1450 50  0001 C CNN
F 3 "~" H 7650 1450 50  0001 C CNN
	1    7650 1450
	1    0    0    -1  
$EndComp
$Comp
L JumperLayout-rescue:Fuse-Device F1
U 1 1 5C34CBDE
P 800 825
F 0 "F1" V 800 825 50  0000 C CNN
F 1 "Fuse" V 875 825 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 730 825 50  0001 C CNN
F 3 "~" H 800 825 50  0001 C CNN
	1    800  825 
	0    1    1    0   
$EndComp
$Comp
L JumperLayout-rescue:Logo-MyLib L1
U 1 1 5C383346
P 6500 7725
F 0 "L1" H 6628 7821 50  0000 L CNN
F 1 "Logo" H 6628 7730 50  0000 L CNN
F 2 "Graphics:KicadLogo" H 6500 7725 50  0001 C CNN
F 3 "" H 6500 7725 50  0001 C CNN
	1    6500 7725
	1    0    0    -1  
$EndComp
Connection ~ 1150 1425
Connection ~ 1150 1125
Wire Wire Line
	1150 1425 1700 1425
Wire Wire Line
	1150 1125 1375 1125
Wire Wire Line
	1700 825  1375 825 
Connection ~ 1700 1125
Connection ~ 1700 825 
Wire Wire Line
	1375 825  1350 825 
Connection ~ 1375 825 
Wire Wire Line
	1700 725  1700 825 
$EndSCHEMATC
